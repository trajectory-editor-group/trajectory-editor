import tredit.config as config
import tredit.data_io as data_io


class Savestate:
    """
    Handles saving and loading for data states.

    :author: Tim Alexis Körner
    """

    def __init__(self, plot, active_tab):
        """
        Initializes the Savestate with a maximum number of saves.
        The saves are internally saved as a list of tuples, each tuple representing a state.

        :param plot: Plot instance for redrawing and figures.
        :param active_tab: Currently active GUI tab, decides which canvas have the current data displayed.
        """
        self.plot = plot
        self.active_tab = active_tab

        self.current_state_number = -1
        self.states = []

    def reset(self):
        """
        Deletes all saves.
        """
        self.current_state_number = -1
        self.states = []

    def save(self, state=None):
        """
        Saves a state and appends them to the saves list. If the maximum amount of saves (see config) is reached,
        discards the oldest state. Creates a new state of the current data if not given one.

        :param state: Already created save state to append. Default is None.
        """
        self.states = self.states[:self.current_state_number + 1]
        if len(self.states) == config.max_save_count:
            self.states = self.states[1:]
        if state is None:
            state = self.create_state()
        self.states.append(state)
        self.current_state_number = len(self.states) - 1

    def create_state(self):
        """
        Create a state tuple of the current data.
        Also included is the current selection range and copied regions of all active canvas.

        :return: State tuple.
        """
        tab_to_regions = {self.active_tab: self.copy_active_regions()}
        return data_io.float_indices.copy(), data_io.dataframe.copy(), tab_to_regions

    def load(self):
        """
        Loads the current state (adjusted beforehand). The loaded state is not
        discarded from the state list to allow future undo and redo load calls.
        After the data is updated, the figures are replotted. (lines recached). Afterwards all canvas
        backgrounds are deleted to trigger a complete draw next time they are drawn. If the currently active tab
        has saved regions with no changes the regions are restored.
        Otherwise the canvas on the current tab are redrawn and the regions are added to the state.
        """
        if self.current_state_number == -1:
            return
        state = self.states[self.current_state_number]
        data_io.float_indices, data_io.dataframe, tab_to_regions = state

        self.plot.replot(draw=False)
        for figure in self.plot.figures:
            figure.canvas.background = None

        if self.active_tab in tab_to_regions.keys():
            for index in range(len(tab_to_regions[self.active_tab])):
                figure, limits, dimensions, size, canvas_region = tab_to_regions[self.active_tab][index]
                if all(size == figure.bbox.size) and dimensions == figure.dimensions and \
                        limits == (figure.axes[0].get_xlim(), figure.axes[0].get_ylim()):

                    figure.canvas.restore_region(canvas_region)
                    figure.canvas.blit(figure.bbox)
                else:
                    figure.scale()
                    figure.canvas.draw(False)
                    tab_to_regions[self.active_tab][index] = self.copy_region(figure)
        else:
            active_regions = self.copy_active_regions(True)
            tab_to_regions.update({self.active_tab: active_regions})

    def copy_active_regions(self, draw=False):
        """
        Copies the canvas-region of all active canvas. Draws beforehand if told to.

        :param draw: Boolean representing if the canvas should be drawn before saving the region. Default is False.
        :return: List of region tuples.
        """
        active_regions = []
        for figure in self.plot.figures:
            if figure.canvas.active:
                if draw:
                    figure.canvas.draw(False)
                region = self.copy_region(figure)
                active_regions.append(region)
        return active_regions

    def copy_region(self, figure):
        """
        Copies a single canvas-region of a given figure. This includes the figure reference itself (no copy)
        for comparisons, the axes limits, the plotted dimensions, the size, and the canvas-region.

        :param figure: Figure to create a region tuple for.
        :return: Single region tuple.
        """
        return figure, (figure.axes[0].get_xlim(), figure.axes[0].get_ylim()),\
            figure.dimensions.copy(), figure.bbox.size.copy(), figure.canvas.copy_from_bbox(figure.bbox)

    def load_undo(self):
        """
        Loads the previous state, if available.
        """
        if self.current_state_number > 0:
            self.current_state_number -= 1
            self.load()

    def load_redo(self):
        """
        Reloads the next state, if available.
        """
        if self.current_state_number < len(self.states) - 1:
            self.current_state_number += 1
            self.load()
