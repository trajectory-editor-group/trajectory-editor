from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as QtCanvas
from matplotlib.patches import Ellipse
from matplotlib.ticker import AutoLocator, MultipleLocator

import tredit.config as config
import tredit.pickercalculations as pickcalc


class Canvas(QtCanvas):
    """
    Own canvas implementation, responsible for canvas movement and zoom and blitting based drawing.
    Inherits FigureCanvasQTAgg.

    :author: Tim Alexis Körner
    """

    def __init__(self, figure):
        """
        Initialisation using given figure. Sets itself as inactive, and unconnected.

        :param figure: Figure of the Canvas.
        """
        super().__init__(figure)
        self.active = False
        self.background = None
        self.connected = False
        self.wheel_press_point = None

    def __str__(self):
        """
        Creates a string representation with figure.

        :return: String representation of this Canvas.
        """
        return 'Canvas for ' + str(self.figure)

    def connect(self):
        """
        If not yet connected, connect to all necessary keyboard and mouse inputs.
        Depends on dimension type 2d / 3d of the figure.
        """
        if self.connected:
            return
        self.connected = True
        if self.figure.is_3d:
            self.connect_action3d()
        else:
            self.connect_action2d()

    def connect_action2d(self):
        """
        Connects all necessary keyboard and mouse inputs for a 2d canvas,
        also connects all selections of the underlying figure to itself.
        """
        self.mpl_connect('button_press_event', self.on_button_press2d)
        self.mpl_connect('motion_notify_event', self.on_motion2d)
        self.mpl_connect('button_release_event', self.on_release2d)
        self.mpl_connect('scroll_event', self.on_scroll2d)
        self.figure.selection.connect(self)

    def connect_action3d(self):
        """
        Connects all necessary keyboard and mouse inputs for a 3d canvas.
        Selections of the underlying figure are not connected in 3d mode.
        """
        self.figure.axes[0].mouse_init(rotate_btn=2, zoom_btn=0)
        self.mpl_connect('button_press_event', self.on_button_press3d)
        self.mpl_connect('button_release_event', self.on_release3d)
        self.mpl_connect('scroll_event', self.on_scroll3d)

    def on_button_press2d(self, event):
        """
        Handles singular / double mouse button clicks (pressed and released) if recorded inside 2d canvas,
        including left double click to select everything, right double click to clear selection,
        mousewheel press and hold to move the canvas and mousewheel double press so reset scale.
        Visual click indication is for each left mouse click, if activated.

        :param event: Press event with associated button.
        """
        # 1: left, 2: wheel, 3: right
        if event.inaxes is None:
            return
        if event.button == 1:
            if event.dblclick:
                self.figure.selection.select_everything()
        elif event.button == 2:
            if event.dblclick:
                self.figure.scale()
                self.draw(False)
            else:
                self.wheel_press_point = (event.xdata, event.ydata)
        elif event.button == 3:
            if event.dblclick:
                self.figure.scale()
                self.draw(False)
            else:
                self.figure.selection.clear_selection()

    def on_motion2d(self, event):
        """
        Handles mouse motion inside 2d canvas, including pressed mousewheel on motion for canvas movement.

        :param event: Motion event.
        """
        if event.inaxes is None:
            return
        if self.wheel_press_point is not None:
            self.move_canvas2d(event)

    def on_release2d(self, event):
        """
        Handles mouse button releases for 2d canvas regardless of position,
        including mousewheel release to stop canvas movement and get the background after movement.

        :param event: Release event with associated button.
        """
        if event.button == 2:
            self.wheel_press_point = None
            self.draw(False)

    def on_scroll2d(self, event):
        """
        Handles mousewheel scrolling inside 2d canvas, including canvas zoom.

        :param event: Scroll event.
        """
        if event.inaxes is None:
            return
        self.zoom2d(event)

    def move_canvas2d(self, event):
        """
        Moves canvas if mouse moves inside 2d canvas while mousewheel is pressed.

        :param event: Motion event with position.
        """
        xlim = self.wheel_press_point[0] - event.xdata + self.figure.axes[0].get_xlim()
        self.figure.axes[0].set_xlim(xlim)
        if not self.figure.is_multiplot:
            ylim = self.wheel_press_point[1] - event.ydata + self.figure.axes[0].get_ylim()
            self.figure.axes[0].set_ylim(ylim)
        self.draw()

    def zoom2d(self, event):
        """
        Zoom in and out of current plot by rescaling axes of the figure.
        If the horizontal axis represents time, only scale along it.

        :param event: Scroll event with direction as button.
        """
        base_scale = 1.2
        if event.button == 'up':
            scale_factor = 1 / base_scale
        elif event.button == 'down':
            scale_factor = base_scale
        else:
            return
        xlim = self.figure.axes[0].get_xlim()
        half_xspan = (xlim[1] + xlim[0]) / 2
        half_xrange = (xlim[1] - xlim[0]) / 2
        self.figure.axes[0].set_xlim([half_xspan - scale_factor * half_xrange,
                                      half_xspan + scale_factor * half_xrange])

        if self.figure.is_multiplot:
            if 2 * half_xrange * config.frequency < 10:
                self.figure.axes[0].xaxis.set_major_locator(MultipleLocator(1 / config.frequency))
            else:
                self.figure.axes[0].xaxis.set_major_locator(AutoLocator())
        else:
            ylim = self.figure.axes[0].get_ylim()
            half_yspan = (ylim[1] + ylim[0]) / 2
            half_yrange = (ylim[1] - ylim[0]) / 2
            self.figure.axes[0].set_ylim([half_yspan - scale_factor * half_yrange,
                                          half_yspan + scale_factor * half_yrange])
        self.draw(False)

    def on_button_press3d(self, event):
        """
        Handles singular / double mouse button clicks (pressed and released)
        if recorded inside 3d canvas, including
        left double click to clear selection, right click to select everything,
        and mousewheel double press so reset scale.

        :param event: Press event with associated button.
        """
        if event.inaxes is None:
            return
        if event.button == 1:
            if event.dblclick:
                self.figure.selection.select_everything()
        elif event.button == 2 and event.dblclick:
            self.figure.scale()
            self.draw(False)
        elif event.button == 3:
            if event.dblclick:
                self.figure.scale()
                self.draw(False)
            else:
                self.figure.selection.clear_selection()

    def on_release3d(self, event):
        """
        Handles mouse button releases for 3d canvas regardless of position,
        including mousewheel release to get the background after movement.

        :param event: Release event with associated button.
        """
        if event.button == 2:
            self.draw(False)

    def on_scroll3d(self, event):
        """
        Handles mousewheel scrolling inside 3d canvas, including canvas zoom.

        :param event: Scroll event.
        """
        if event.inaxes is None:
            return
        self.zoom3d(event)

    def zoom3d(self, event):
        """'
        Zoom in and out of current plot using distance property of 3d axes.

        :param event: Scroll event with direction as button.
        """
        if event.inaxes is None:
            return
        base_scale = 1.2
        old_dist = self.figure.axes[0].dist
        if event.button == 'up':
            self.figure.axes[0].dist = old_dist / base_scale
        elif event.button == 'down':
            self.figure.axes[0].dist = old_dist * base_scale
        self.draw()

    def draw(self, qt_call=True):
        """
        Calls inherited draw function. If not called from Qt (e.g. Window rescaling), this method
        saves a background (only figure axis with ticks, modulo lines) after hiding most lines and drawing.
        The background is used for future blitting of the quickdraw method (which is subsequently called once).
        Qt intern draw calls of current Version (5.11) with subsequent blitting result in recursion as blitting calls
        for a Qt paint event, which in turn calls draw. In order not to draw twice (once with minimal objects
        to save a background and a second time for all objects) Qt intern draw calls do not save a background,
        but instead deletes it. If the canvas is inactive (not on the current tab) no drawing will be done and
        background is reset.

        :param qt_call: Catches Qt intern draw calls. Default is True.
        """
        if not self.active:
            self.background = None
            return

        visible_artists = []
        if not qt_call:
            for line in self.figure.axes[0].get_lines():
                if line.get_visible():
                    visible_artists.append(line)
                    line.set_visible(False)
            for selection_marker_group in self.figure.selection_marker_groups:
                for selection_marker in selection_marker_group:
                    if selection_marker.get_visible():
                        visible_artists.append(selection_marker)
                        selection_marker.set_visible(False)
        else:
            self.background = None

        super().draw()

        if not qt_call:
            self.background = self.copy_from_bbox(self.figure.bbox)
            for artist in visible_artists:
                artist.set_visible(True)
            self.quickdraw()

    def quickdraw(self):
        """
        Uses the previously saved backgound from draw (if no background available calls draw itself) to draw quickly.
        This is achieved by blitting (adding) all visible objects (lines, shapes, etc.)
        onto an already existing background. If the Canvas is inactive, nothing will be done.
        """
        if not self.active:
            return
        if self.background is None:
            self.draw(False)
            return

        self.restore_region(self.background)
        for line in self.figure.axes[0].get_lines():
            self.figure.axes[0].draw_artist(line)
        for selection_marker_group in self.figure.selection_marker_groups:
            for selection_marker in selection_marker_group:
                if selection_marker.axes is not None:
                    selection_marker.draw_artist()
        self.blit(self.figure.bbox)
