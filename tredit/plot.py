import tredit.config as config
import tredit.data_io as data_io
from tredit.figure import Figure
from tredit.savestate import Savestate
from tredit.selection import Selection


class Plot:
    """
    Class containing and plotting related methods. Creates Figures according to given GUI structure.
    ALso distributes plot, replot, scale, draw calls and updates active status of each Canvas on tab change.

    :author: Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates Savestate, Selection and all necessary Figures. The Figures are saved in a list for simpler iteration.

        :param gui: Handed to Selection to check for active filter widgets.
        """
        active_tab = list(config.structure.keys())[0]
        self.savestate = Savestate(self, active_tab)

        self.selection = Selection(self, gui)
        self.figures = []
        self.create_figures(active_tab)

    def create_figures(self, active_tab):
        """
        Creates all necessary Figures. The Figure type (multiplot, 2d/3d) is decided by the dims to be plotted on the
        Figure in the future. If the horizontal axis will represent the time and there are only two axis needed, the
        Figure will be a multiplot. The currently active tab will also be passed to the Figure. If all to be plotted
        dims use the same modulo along any axis additional modulo border indication lines will be added at -mod, 0, mod.
        Finally the figure placeholders in the GUI structure dictionary will be replaced by the real Figures.

        :param active_tab: Currently active tab.
        """
        for tab, figure_to_plot_attributes in config.structure.items():
            for temp_figure, plot_attributes in list(figure_to_plot_attributes.items()):

                dimensions = plot_attributes['dimensions']
                is_3d = max(len(dims) for dims in dimensions) == 3
                is_multiplot = not is_3d and set(dims[0] for dims in dimensions) == set('t')
                figure = Figure(tab, self.selection, is_3d, is_multiplot)
                figure.canvas.active = (tab == active_tab)
                if not is_3d:
                    for dim_index in range(2):
                        modulos = set()
                        for dims in dimensions:
                            if 'modulo' in config.dim_to_property[dims[dim_index]]:
                                modulos.add(config.dim_to_property[dims[dim_index]]['modulo'])
                        if len(modulos) == 1:
                            mod = modulos.pop()
                            for line_pos in [-mod, 0, mod]:
                                if dim_index == 0:
                                    figure.axes[0].axvline(line_pos, lw=0.5, ls='--', color='gray')
                                else:
                                    figure.axes[0].axhline(line_pos, lw=0.5, ls='--', color='gray')

                figure_to_plot_attributes[figure] = figure_to_plot_attributes.pop(temp_figure)
                self.figures.append(figure)

    def plot(self):
        """
        Plots all dimensions from the GUI structure dictionary, draws all canvas and saves the current state.
        """
        self.savestate.reset()
        self.selection.clear_selection(draw=False)

        for tab, figure_to_plot_attributes in config.structure.items():
            for figure, plot_attributes in figure_to_plot_attributes.items():
                figure.plot(plot_attributes['dimensions'])

        self.draw()
        self.savestate.save()

    def replot(self, dims=None, draw=True):
        """
        Replots all figures by using the replot method of each figure.

        :param dims: Single dimensions to be replotted, if None all single dimensions are replotted. Default is None.
        :param draw: Determines if the plot should be redrawn afterwards. Default is True.
        """
        if dims is None:
            dims = list(config.dim_to_property.keys())
        function_dims = data_io.calc_dim_functions(dims)
        self.selection.recalculate_position(draw=False)
        for figure in self.figures:
            figure.replot(dims+function_dims, draw)

    def scale(self):
        """
        Rescales all figures by using the scale method of each figure.
        """
        for figure in self.figures:
            figure.scale()

    def draw(self, qt_call=False):
        """
        Draws the canvas of every figure by using the draw method of each canvas.

        :param qt_call: Determines if draw call should be handled as if it came from Qt. Default is False.
        """
        for figure in self.figures:
            figure.canvas.draw(qt_call)

    def quickdraw(self):
        """
        Quickdraws the canvas of every figure by using the quickdraw method of each canvas.
        """
        for figure in self.figures:
            figure.canvas.quickdraw()

    def on_tab_change(self, active_tab):
        """
        Changes currently active tab and redraws all associated canvas using quickdraw.

        :param active_tab: Tab the GUI got changed to.
        """
        self.savestate.active_tab = active_tab
        for figure in self.figures:
            figure.canvas.active = (figure.tab == active_tab)
        self.quickdraw()
