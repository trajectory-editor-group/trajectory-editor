from matplotlib.figure import Figure as MatplotFigure
from mpl_toolkits.mplot3d.art3d import Line3D
from matplotlib.lines import Line2D
from matplotlib.ticker import AutoLocator, MultipleLocator

import tredit.config as config
import tredit.data_io as data_io
from tredit.canvas import Canvas


class Figure(MatplotFigure):
    """
    Own Figure implementation for simplified Multiplot and 3d plots, selection adaptions,
    dimension switching, efficient replotting and scaling adjustments.
    Inherits Figure from matplotib.figure.
    The plot function should be used for data set changes, otherwise the replot function is sufficient.

    :author: Tim Alexis Körner
    """

    def __init__(self, tab, selection, is_3d, is_multiplot):
        """
        Initialisation according to dimension type.
        Disables matplotib autoscaling and creates canvas for itself.

        :param tab: Tab of the GUI where the Figure is located.
        :param selection: Shared selection with all other Figures.
        :param is_3d: Determines if there will be 3d or 2d plots for this Figure. This is not done during plot call to\
        prevent issues regarding Qt displaying canvas before plotting any data.
        :param is_multiplot: Determines if there will be multiple plots against time, which corresponds to the vertical\
        bar selection type.
        """
        super().__init__(tight_layout=True)
        self.tab = tab
        self.selection = selection
        self.selection_marker_groups = []
        self.is_3d = is_3d
        self.is_multiplot = is_multiplot

        self.canvas = Canvas(self)

        if not is_3d:
            self.gca()
        else:
            self.gca(projection='3d')
            self.axes[0].mouse_init(rotate_btn=0, zoom_btn=0)
        self.axes[0].autoscale(enable=False)
        self.axes[0].tick_params(labelsize=config.mpl_fontsize)
        self.dims_to_lines = {}
        self.dim_to_dim_limits = {}
        self.dimensions = []

    def __str__(self):
        """
        Creates a string representation with tab, 2d or 3d and multiplot identifier,
        currently displayed and all dimensions.

        :return: String representation of this Figure.
        """
        figure_string = 'Figure from tab ' + self.tab
        if self.is_3d:
            figure_string + ' (3d)'
        else:
            figure_string + ' (2d)'
        if self.is_multiplot:
            figure_string + ' (multiplot)'
        figure_string += ', current dimensions: ' + str(self.dimensions)
        figure_string += ', all dimensions: ' + str(list(self.dims_to_lines.keys()))
        return figure_string

    def plot(self, dimensions):
        """
        Creates new lines for each dimensions and adds gives the Figure a selection.
        Also sets the first displayed dimensions which are dependent on the Figure type and may differ to the given one.
        This also scales the axis. Connects the canvas with user input.

        :param dimensions: A list of all dims to be displayed on the Figure.
        """
        if self.has_plot():
            self.reset()
        self.create_lines(dimensions)
        self.create_selection(dimensions)
        self.set_labels(dimensions)
        self.set_plot(dimensions, draw=False)
        self.canvas.connect()

    def has_plot(self):
        """
        Determines if the Figure currently has a plot.

        :return: Boolean representing if the Figure has a plot.
        """
        return self.dims_to_lines != {}

    def reset(self):
        """
        Removes all saved lines and selection markers.
        """
        for start_point, line in self.dims_to_lines.values():
            if line.axes is not None:
                start_point.remove()
                line.remove()
        self.dims_to_lines.clear()

        for selection_marker_group in self.selection_marker_groups:
            self.selection.remove_markers(selection_marker_group)
        self.selection_marker_groups = []

    def create_lines(self, dimensions):
        """
        Creates a start point and 2d/3d line for every dims in the given dimensions with the matplotlib line keyword
        arguments of the second dim if the plot is a multiplot. The start point will always overwrite
        the marker to represent a dot. Since non multiplots will have a lines dependent on multiple (non time) dims
        all keyword arguments will be disregarded, and the line will have the color black.

        :param dimensions: Dimensions for which lines should be created.
        """
        for i in range(len(dimensions)):
            dims = dimensions[i]
            dims_data = self.get_data(dims)

            kwargs = {'color': 'black'}
            if self.is_multiplot:
                property_to_value = config.dim_to_property[dims[-1]].copy()
                property_to_value.pop('modulo', None)
                property_to_value.pop('unit', None)
                kwargs.update(property_to_value)

            if not self.is_3d:
                line = Line2D(*dims_data, **kwargs)
                kwargs['marker'] = 'o'
                start_point = Line2D(*[[dim_data[0]] for dim_data in dims_data], **kwargs)
            else:
                line = Line3D(*dims_data, **kwargs)
                kwargs['marker'] = 'o'
                start_point = Line3D(*[[dim_data[0]] for dim_data in dims_data], **kwargs)
            self.dims_to_lines.update({dims: (start_point, line)})

    def create_selection(self, dimensions):
        """
        Assigns selections markers according to the possible dims of this Figure.
        If the figure is a multiplot only one trio of selection markers will be needed.

        :param dimensions: Dimensions for which selections markers should be created.
        """
        if self.is_multiplot:
            dimensions = [dimensions[0]]
        for dims in dimensions:
            selection_marker_group = self.selection.add_figure(self, dims)
            self.selection_marker_groups.append(selection_marker_group)

    def set_labels(self, dimensions):
        """
        Sets the static labels along each axis according to the initial dimensions.
        All dynamic changes occur via labeled checkboxes.
        Will add unit symbols and reverse the order along the vertical axis to be more natural.
        """
        horizontal_dims = set(dims[0] for dims in dimensions)
        vertical_dims = set(dims[1] for dims in dimensions)

        def add_unit(dim_label):
            if 'unit' in config.dim_to_property[dim_label]:
                unit = config.dim_to_property[dim_label]['unit']
                dim_label += ' [' + unit + ']'
            return dim_label

        if len(horizontal_dims) == 1:
            self.axes[0].set_xlabel(add_unit(horizontal_dims.pop()), fontsize=config.mpl_fontsize)

        if len(vertical_dims) == 1:
            self.axes[0].set_ylabel(add_unit(vertical_dims.pop()), fontsize=config.mpl_fontsize)

        if self.is_3d:
            self.axes[0].set_zlabel(add_unit(dimensions[0][2]), fontsize=config.mpl_fontsize)

    def get_data(self, dims):
        """
        Looks up the data of all dims and bundles them in a list. Also calculates the limits of the data of each dim.

        :param dims: Multiple dim for which the data should be bundled for.
        :return: Data bundle.
        """
        dims_data = []
        for dim in dims:
            dim_data = data_io.get_dim_data(dim, calculate_modulo=True)
            dims_data.append(dim_data)
            self.dim_to_dim_limits[dim] = (min(dim_data), max(dim_data))
        return dims_data

    def set_plot(self, dimensions, draw=True):
        """
        Sets plot by adding all given dims to the Figure and hiding all other dimensions.
        If the Figure is a multiplot, only the first dims will be used.
        Also sets axis labels and scales.
        Draws if not told otherwise.

        :param dimensions: Multiple dims to be displayed.
        :param draw: Determines if the figure should be drawn after setting the dims. Default is True.
        """
        if not self.is_multiplot:
            dimensions = [dimensions[0]]
        self.dimensions = dimensions

        for dims, (start_point, line) in self.dims_to_lines.items():
            if dims in self.dimensions:
                if line.axes is None:
                    self.axes[0].add_line(start_point)
                    self.axes[0].add_line(line)
            elif line.axes is not None:
                start_point.remove()
                line.remove()

        self.set_selection()
        self.scale()
        if draw:
            self.canvas.draw(False)

    def set_selection(self):
        """
        Sets the selection markers of the Figure to the currently displayed dimensions.
        If the Figure is a multiplot nothing will be done since there will be only one trio.
        """
        if self.is_multiplot:
            return
        for selection_marker_group in self.selection_marker_groups:
            if selection_marker_group[0].dims in self.dimensions:
                self.selection.add_markers(selection_marker_group, self.axes[0])
            else:
                self.selection.remove_markers(selection_marker_group)

    def replot(self, dims, draw=True):
        """
        Recaches all affected lines by the changed dims but only draws when a currently displayed dimension was changed.

        :param dims: Changed dims to be replotted.
        :param draw: Determines if the Figure should be drawn if needed. Default is True.
        """
        recached = self.recache_plot(dims)

        if recached and draw:
            for changed_dim in dims:
                for plotted_dims in self.dimensions:
                    if changed_dim in plotted_dims:
                        self.canvas.quickdraw()
                        return

    def recache_plot(self, dims):
        """
        Recaches the all dims that were changed and can be displayed on the Figure.
        By resetting all data of the affected lines and calling the recache on them.

        :param dims: Changed dims to be recached.
        :return: Boolean representing if a recache was done.
        """
        recached = False
        for possible_dims, (start_point, line) in self.dims_to_lines.items():
            for changed_dim in dims:
                if changed_dim in possible_dims:
                    dims_data = self.get_data(possible_dims)
                    if not self.is_3d:
                        line.set_data(*dims_data)
                        start_point.set_data(*[[dim_data[0]] for dim_data in dims_data])
                    if self.is_3d:
                        line.set_data_3d(*dims_data)
                        start_point.set_data_3d(*[[dim_data[0]] for dim_data in dims_data])
                    start_point.recache(True)
                    line.recache(True)
                    recached = True
                    break
        return recached

    def scale(self):
        """
        Scales the axes with 8% margin on both sides and resets view orientation for 3d plots.
        Works with previously calculated dim data limits.
        Replaces autoscaling of matplotlib and must be manually called.
        """
        if self.is_3d:
            dim_range = range(3)
        else:
            dim_range = range(2)

        limits = []
        for dim_index in dim_range:
            dim_min = 0
            dim_max = 0
            if self.dimensions:
                dim_min = min(self.dim_to_dim_limits[dims[dim_index]][0] for dims in self.dimensions)
                dim_max = max(self.dim_to_dim_limits[dims[dim_index]][1] for dims in self.dimensions)
            if not self.dimensions or dim_min == dim_max:
                dim_min = -1
                dim_max = 1
            margin = (dim_max - dim_min) * 0.08
            limits.append((dim_min - margin, dim_max + margin))

        if self.is_multiplot:
            if (limits[0][1] - limits[0][0]) * config.frequency < 10:
                self.axes[0].xaxis.set_major_locator(MultipleLocator(1 / config.frequency))
            else:
                self.axes[0].xaxis.set_major_locator(AutoLocator())

        self.axes[0].set_xlim(limits[0])
        self.axes[0].set_ylim(limits[1])
        if self.is_3d:
            self.axes[0].set_zlim(limits[2])
            self.axes[0].azim = -60
            self.axes[0].elev = 30
            self.axes[0].dist = 10
