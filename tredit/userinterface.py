import os
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

import tredit.config as config
import tredit.data_io as data_io
from tredit.plot import Plot
import tredit.widgets as widgets


class TabWidget(QTabWidget):
    """
    Creates a QTabWidget that will be used later in the UI. This widget will contain all the other graph widgets.
    The Tabs structure can be found in config.py.
    The content widgets the TabWidget are SwitchViewWidget and MultiPlotWidget.

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, parent):
        """
        When initialized a widget for every figure in the structure dictionary will be created at the
        specified position and tab. Each figure has a canvas where the data will be plotted on.
        Notifies the plots on tab changes.

        :param parent: The GUI that embeds the Tabs.
        """

        super(QTabWidget, self).__init__(parent)
        self.plot = parent.plot
        self.button_widgets = []
        for tab_name in config.structure.keys():
            self.addTab(tab_name)
        self.setStyleSheet('font: ' + str(config.qt_fontsize + 2) + 'px; color: black')

        self.currentChanged.connect(lambda tab_number: self.plot.on_tab_change(self.get_active_tab()))

    def addTab(self, tab_name):
        """
        Adds the given tab. For each figure in the tab the embedding widget (SwitchViewWidget / MultiPlotWidget)
        or none (direct canvas) is determined. The widgets are saved for later enabling (after loading data).
        Since the figures do not have to be sorted in the config structure dictionary, their widgets are first ordered
        by the second position (y / row) and then by the first position (x / column). This order is then used to
        place them in a horizontal layout consisting of multiple vertical layouts. The QGridLayout is not used to keep
        the position simple with only (x,y).

        :param tab_name: Tab to add, name is displayed on the top.
        """
        unsorted_widgets = []

        for figure, plot_attributes in config.structure[tab_name].items():
            if figure.is_3d:
                widget = figure.canvas
            else:
                if not figure.is_multiplot:
                    widget = widgets.SwitchViewWidget(figure, plot_attributes['dimensions'])
                else:
                    primary_dim = plot_attributes['dimensions'][0][0]
                    secondary_dims = [dims[1] for dims in plot_attributes['dimensions']]
                    widget = widgets.MultiPlotWidget(figure, primary_dim, secondary_dims)
                self.button_widgets.append(widget)

            unsorted_widgets.append((widget, plot_attributes['position']))

        sorted_widgets = sorted(sorted(unsorted_widgets, key=lambda w: w[1][1]), key=lambda w: w[1][0])

        hlayout = QHBoxLayout()
        vlayout = QVBoxLayout()
        last_column = -1
        for widget, position in sorted_widgets:
            current_column = position[0]
            if last_column != current_column:
                if last_column != -1:
                    hlayout.addLayout(vlayout)
                vlayout = QVBoxLayout()
                last_column = current_column
            vlayout.addWidget(widget)

        hlayout.addLayout(vlayout)
        tab = QWidget()
        tab.setLayout(hlayout)
        super().addTab(tab, tab_name)

    def enable_boxes_buttons(self):
        """"
        Activates the checkboxes and radio-buttons of every SwitchViewWidget and MultiPlotWidget in the tab.
        This is to avoid problems if the user clicks on any box before data is loaded.
        """
        for widget in self.button_widgets:
            widget.enable()

    def get_active_tab(self):
        """
        Returns the currently active tab.

        :return: Tab name.
        """
        return self.tabText(self.currentIndex())


class GUI(QMainWindow):
    """
    Graphic User Interface. Consists of a Tabwidget, Menubar and a Statusbar. Contains functions for opening saving and
    simulating a file.
    The general characteristics and contents of these can be changed here. Fist instances of the classes Plot and
    TabWidget are created. Then all the filter windows
    are created and collected in a list. The Menubar with its dropdown menus and shortcuts get created which is followed
    by the creation of the status bar.

    :author: Henrik Trommer
    """

    def __init__(self):
        """
        Creating the actions for the menubars dropdown menus and creating the statusbar with a timer
        to reset it after it gets called. Further the menubar ist getting created adn initialized and the TabWidget gets
        set as the main- widget.
        Moreover after starting the application the user will see a dialog to chose a file for opening.
        """
        super().__init__()

        # init Plot and TabWidget
        self.filter_widgets = []
        self.plot = Plot(self)
        self.tabwidget = TabWidget(self)

        # creating the actions for the menubars dropdown menus
        # for the dropdown 'File':
        self.openFile = QAction('&Open File...', self)
        self.saveFile = QAction('&Save', self)
        self.saveFileAs = QAction('&Save As...', self)
        self.help = QAction('&Help...', self)
        self.exitMe = QAction('&Exit', self)
        # for the dropdown 'Edit':
        self.undo = QAction('&Undo', self)
        self.redo = QAction('&Redo', self)
        # for the dropdown 'Simulation':
        self.start_sim = QAction('&Start Simulation', self)
        self.choose_file_for_simulation = QAction('&Chose File', self)

        self.menubar_functions()

        # creating the statusbar with a timer to reset it after it gets called
        self.statusBar = self.statusBar()
        self.message_Timer = QTimer()
        self.message_Timer.timeout.connect(self.set_statusbar_text_black)
        self.message_Timer.setSingleShot(True)

        # adding dialog windows for the filters
        self.filter_widgets.append(widgets.LinearFilterDialog(self))
        self.filter_widgets.append(widgets.ExpSmoothingFilterDialog(self))
        self.filter_widgets.append(widgets.SavitzkyGolayFilterDialog(self))
        self.filter_widgets.append(widgets.LevelFilterDialog(self))
        self.filter_widgets.append(widgets.CutFilterDialog(self))
        self.filter_widgets.append(widgets.SpeedFilterDialog(self))
        self.filter_widgets.append(widgets.StopFilterDialog(self))
        self.filter_widgets.append(widgets.AmplificationFilterDialog(self))
        self.filter_actions = QActionGroup(self)

        # creating and initializing the menubar
        self.menubar = self.menuBar()
        self.init_menu_bar()
        # setting GUI window size, and title
        self.setGeometry(50, 50, 1000, 500)
        self.setWindowTitle("Trajectory Editor")
        self.setMinimumSize(800, 600)

        # here we set the TabWidget as the main- widget
        self.setCentralWidget(self.tabwidget)
        self.setWindowIcon(QIcon('tredit/icon.png'))
        self.show()

        # after starting the application the user will see a dialog to chose a file for opening
        self.open_filename_dialog()

    def set_statusbar_text_black(self):
        """
        Sets the text color in the statusbar to black.
        """
        self.statusBar.setStyleSheet('font: bold ' + str(config.qt_fontsize) + 'px; color: black')

    def show_statusbar_message(self, text, color):
        """
        Displays a message in the statusbar.

        :param text: message that will be displayed
        :param color: text color
        :param msecs: time for that the message will be displayed
        """
        self.statusBar.setStyleSheet('font: bold ' + str(config.qt_fontsize) + 'px; color: ' + color)
        self.statusBar.showMessage(text, config.statusbar_timer)
        self.message_Timer.start(config.statusbar_timer)

    def closeEvent(self, event):
        """
        Creates a small Window where the user has to confirm that he wants to exit.

        :param event: Is the close Event
        """
        if not config.show_exit_window:
            event.accept()
            return

        quit_window = QMessageBox()
        quit_window.setWindowTitle('Quit')
        quit_window.setText("Are you sure?")
        quit_window.setStandardButtons(QMessageBox.Yes | QMessageBox.Cancel)
        quit_window.setWindowIcon(QIcon('tredit/icon.png'))
        quit_result = quit_window.exec()

        if quit_result == QMessageBox.Yes:
            for filter_widget in self.filter_widgets:
                filter_widget.close()

            event.accept()
        else:
            event.ignore()

    def init_menu_bar(self):
        """
        Initialises the menubar by creating dropdown menus 'File', 'Edit', 'Functions' (filters), and 'Simulation'
        and to them. Additionally shortcut actions and statustips are added for the filters.
        """
        # creating dropdown 'File' and adding actions:
        file = self.menubar.addMenu('&File')
        file.addAction(self.openFile)
        file.addAction(self.saveFile)
        file.addAction(self.saveFileAs)
        file.addAction(self.help)
        file.addAction(self.exitMe)
        # creating dropdown 'Edit' and adding actions:
        edit = self.menubar.addMenu('&Edit')
        edit.addAction(self.undo)
        edit.addAction(self.redo)
        # creating dropdown 'Functions' and adding actions:
        filter_menu = self.menubar.addMenu('&Functions')
        self.filter_actions.triggered.connect(self.on_filter_click)
        # creating dropdown 'Simulation' and adding actions:
        simulation = self.menubar.addMenu('&Simulation')
        simulation.addAction(self.start_sim)
        simulation.addAction(self.choose_file_for_simulation)

        # adding shortcuts and status tips to the filter actions and establishing triggers
        for number, filter_widget in enumerate(self.filter_widgets):
            action = QAction(str(filter_widget), self)
            action.setShortcut('Ctrl+' + str(number + 1))
            action.setStatusTip(filter_widget.statusTip())
            self.filter_actions.addAction(action)
            filter_menu.addAction(action)

    def on_filter_click(self, action):
        """
        Opens the functions that belongs to the actions index. The indices are the same as they are in the
        'functions' (filters) dropdown menu.

        :param action: Keyboard- Shortcut.
        """
        index = self.filter_actions.actions().index(action)
        self.open_filterdialog(self.filter_widgets[index])

    def has_open_filter(self):
        """
        Checks if any filter widget is currently active.

        :return: Boolean status
        """
        for filter_widget in self.filter_widgets:
            if filter_widget.isVisible():
                return True
        return False

    def menubar_functions(self):
        """
        When this function is called, every menubar action (except the filters) is connected to a shortcut,
        status tip and a method which is called if the action is selected.
        """
        self.openFile.setShortcut('Ctrl+O')
        self.openFile.setStatusTip('Open')
        self.openFile.triggered.connect(self.open_filename_dialog)

        self.saveFile.setShortcut('Ctrl+S')
        self.saveFile.setStatusTip('Save')
        self.saveFile.triggered.connect(self.save_file_override)

        self.saveFileAs.setShortcut('Ctrl+Shift+S')
        self.saveFileAs.setStatusTip('Save As')
        self.saveFileAs.triggered.connect(self.save_asfile_dialog)

        self.help.setShortcut('Ctrl+H')
        self.help.setStatusTip('Help')
        self.help.triggered.connect(self.open_manual)

        self.exitMe.setShortcut('Ctrl+C')
        self.exitMe.setStatusTip('Exit')
        self.exitMe.triggered.connect(self.close)

        self.undo.setShortcut('Ctrl+Z')
        self.undo.setStatusTip('Undo')
        self.undo.triggered.connect(self.plot.savestate.load_undo)

        self.redo.setShortcut('Ctrl+Shift+Z')
        self.redo.setStatusTip('Redo')
        self.redo.triggered.connect(self.plot.savestate.load_redo)

        self.start_sim.setShortcut('Ctrl+Alt+1')
        self.start_sim.setStatusTip('Starting Gazebo Simulation of latest opened/saved file.')
        self.start_sim.triggered.connect(self.run_simulation)

        self.choose_file_for_simulation.setShortcut('Ctrl+Alt+2')
        self.choose_file_for_simulation.setStatusTip('Choose the trajectory you want to simulate by Gazebo')
        self.choose_file_for_simulation.triggered.connect(self.open_file_for_simulation)

    def open_manual(self):
        """
        Shows the user manual pdf-file.
        """
        try:
            os.system("xdg-open ./Manual.pdf")
        except Exception as e:
            print(e)
            self.show_statusbar_message("Manual not found", "red")

    def open_filterdialog(self, filter):
        """
        When a filter is selected this function shows a dialog for further options before the filter gets applied.
        It can only be called if a file has been opened, a selection has been made and no other filter is currently in use.
        If not the status bar will display an error message.

        :param filter: filter dialog widget
        """
        if not data_io.has_selection():
            self.show_statusbar_message("Please select data!", "red")
        elif self.has_open_filter():
            self.show_statusbar_message("Another filter is already active!", "red")
        else:
            filter.show()

    def open_filename_dialog(self):
        """
        Opens a dialog where the user can open data files. However the files need to have the right format which is
        13 columns. If the csv file has the right format the file will be opened and its path will be shown in the
        status bar. If not an error message will be displayed.
        """
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(self, "Open File", "",
                                                   "All Files (*);;CSV Files (*.csv);;DAT Files (*.dat);;TXT Files (*.txt)",
                                                   options=options)
        if file_name:
            try:
                data_io.read_data(file_name)
                self.show_statusbar_message("Opened: " + file_name, "green")
            except Exception as e:
                print(e)
                self.show_statusbar_message("Invalid data. Please select a csv file with 13 columns!", "red")
                return
            self.plot.plot()
            self.tabwidget.enable_boxes_buttons()

    def run_simulation(self):
        """
        Runs the trajectory simulation of your latest saved file in Gazebo. If there is no latest saved/opened file an
        error message will be displayed in the status bar.
        """
        if data_io.dataframe is None:
            self.show_statusbar_message("Simulation without data not possible!", "red")
            return

        try:
            data_io.start_simulation()
            self.show_statusbar_message("Starting simulation", "green")
        except Exception as e:
            print(e)
            self.show_statusbar_message("Problem while trying to execute \"" + config.simulation_path + "\"", "red")

    def open_file_for_simulation(self):
        """
        Opens a dialog where the user can open a valid csv file to simulate the trajectory with Gazebo. However the file
        needs to have the right format which is 13 columns. If not, an error message will be displayed in the
        status bar. If the format of selected file is correct the filepath will be shown in the status bar.
        """
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getOpenFileName(self, "Open File for Simulation", "./data",
                                                   "All Files (*);;CSV Files (*.csv);;DAT Files (*.dat)",
                                                   options=options)
        if file_name:
            print(file_name)
            try:
                data_io.start_simulation(file_name)
                self.show_statusbar_message("Starting simulation of: " + file_name, "green")
            except Exception as e:
                print(e)
                self.show_statusbar_message("Invalid data. Please select a csv file with 13 columns!", "red")
                return

    def save_file_override(self):
        """
        This Method simply overrides the latest opened/saved file with the users work status. It also displays a text
        message within the status- bar.
        """
        if data_io.dataframe is None:
            self.show_statusbar_message("No data to save", "blue")
            return
        paths = data_io.output_data()
        for path in paths:
            print(path)
        self.show_statusbar_message("Saved", "green")

    def save_asfile_dialog(self):
        """
        This Method opens a window where the user can decide under which name and filepath he wants to save his work.
        It also displays a text message within the status- bar.
        """
        if data_io.dataframe is None:
            self.show_statusbar_message("No data to save", "blue")
            return
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        file_name, _ = QFileDialog.getSaveFileName(self, "Save As", "./data/trajectory_edit.csv",
                                                   "All Files (*);;CSV Files (*.csv);;DAT Files (*.dat)",
                                                   options=options)
        if file_name:
            paths = data_io.output_data(file_name)
            for path in paths:
                print(path)
            self.show_statusbar_message("Saved as: " + file_name, "green")
        else:
            self.show_statusbar_message("Invalid file name", "red")
