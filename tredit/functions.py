import numpy as np
import scipy.signal
import scipy.interpolate
from functools import wraps


class DimFunctions:
    """
    Class of additional custom additional plotted functions.
    The function decorator simplifies usage of these functions and must be added.
    New functions get a single argument: a 2D numpy array with the specified dimension data as columns
    (specifications in structure dict in config.py). For an example see the speed function v in source code.

    :author: Tim Alexis Körner
    """
    dim_to_function_and_args = {}
    frequency = None

    @staticmethod
    def dim_function_decorator(f):
        """
        Decorator for simplified usage of custom functions.

        :param f: Custom function.
        :return: Wrapped custom function.
        """
        @wraps(f)
        def wrapper(*args, **kwargs):
            if not kwargs:
                args_str = ''.join(str(arg) + ',' for arg in args)[:-1]
                function_dim = f.__name__ + '(' + args_str + ')'
                DimFunctions.dim_to_function_and_args[function_dim] = (f, args)
                return function_dim
            else:
                return f(*args)
        return wrapper

    @staticmethod
    @dim_function_decorator.__func__
    def v(dims_array):
        """
        Function which calculates the speed along the trajectory.

        :param dims_array: Array in the shape (#timeticks in data, #specified dimensions)
        :return: New array with shape (#timeticks in data, 1)
        """
        v_array = np.sum((dims_array[:-1] - dims_array[1:])**2, axis=1)
        v_array = np.append(v_array, v_array[-1])
        return np.sqrt(v_array)/DimFunctions.frequency


class FilterFunctions:
    """
    Class of functions for data manipulation. Each function gets called from the GUI with parameters,
    changes the data and returns the changed data together with indices.
    Each function has an own description how it works.
    Some functions change all dimension of the data. If the don't they get the dimensions that get changed as
    additional parameter. All functions get Indices as parameter and return indices, even if they are unchanged.

    Author: Fabian Schabram

    """
    @staticmethod
    def linear(dims_array, indices):
        """
        Replaces the movement between 2 points with a linear function

        :param dims_array: the array that will be worked on
        :param indices: start and end point between which the function operates
        :return: the changed function
        """
        dims_array_slice = dims_array[indices[0]: indices[1]+1]
        slice_len = dims_array_slice.shape[0]
        slope = (dims_array_slice[-1] - dims_array_slice[0]) / slice_len
        for i in range(slice_len):
            dims_array_slice[i] = dims_array_slice[0] + slope * i

        return dims_array, indices

    @staticmethod
    def exponential_smoothing(dims_array, indices, parameter):
        """
        Smoothes the function by recalculating each data point based on the actual value and the last value.
        gets calculated twice with different directions and a mean value is used, to make the operation symmetrical.

        :param dims_array: the array that will be worked on
        :param indices: indices: start and end point between which the function operates
        :param parameter: function parameter between 0 and 1 that smoothens the function more the bigger it is
        :return: the changed array.
        """
        dims_array_slice = dims_array[indices[0]: indices[1]+1]
        slice_len = dims_array_slice.shape[0]
        dim_data_lr = np.empty(dims_array_slice.shape)
        dim_data_rl = np.empty(dims_array_slice.shape)
        dim_data_lr[0] = dims_array_slice[0]
        dim_data_rl[-1] = dims_array_slice[-1]

        for i in range(1, slice_len):
            dim_data_lr[i] = parameter * dim_data_lr[i - 1] + (1 - parameter) * dims_array_slice[i]
        for i in reversed(range(0, slice_len - 1)):
            dim_data_rl[i] = parameter * dim_data_rl[i + 1] + (1 - parameter) * dims_array_slice[i]

        dims_array[indices[0]: indices[1]+1] = (dim_data_lr + dim_data_rl) / 2

        return dims_array, indices

    @staticmethod
    def speed(dims_array, indices, factor, order=3):
        """
        Filter that speeds up or slows down the movement in a selected timeframe depending on a paramter. uses variable
        polynomial order for spline interpolation to calculate each point.

        :param dims_array: original array
        :param indices: start and end point between which the function changes the speed
        :param factor: factor between 0.1 and 10 that determines how much the movement is faster or slower
        :param order: polynomial order for the spline segments
        :return: the changed array
        """
        left_slice, middle_slice, right_slice = np.split(dims_array, [indices[0], indices[1]+1], axis=0)
        old_len = middle_slice.shape[0]
        new_len = max(2, int(old_len/factor))
        indices[1] = indices[0] + new_len - 1
        new_middle_slice = np.empty((new_len, middle_slice.shape[1]))

        x_old = np.linspace(0, old_len - 1, num=old_len)
        x_new = np.linspace(0, old_len - 1, num=new_len)
        for i in range(middle_slice.shape[1]):
            spline = scipy.interpolate.splrep(x_old, middle_slice[:, i], k=int(order))
            new_middle_slice[:, i] = scipy.interpolate.splev(x_new, spline)

        dims_array = np.vstack((left_slice, new_middle_slice, right_slice))
        return dims_array, indices

    @staticmethod
    def savitzky_golay(dims_array, indices, polyorder):
        """
        Filterfunction using a Savitzky-Golay Filter, to change datapoints. cuts the array in three slices,
        smoothes the data in the middle between indices and filters it

        :param dims_array:  array with all the position data
        :param indices: begin and end of filter application
        :param polyorder: order of the polynom the SG-filter uses
        :return: the new data values and the unchanged indices
        """
        int_polyorder = int(polyorder)
        window_length = int_polyorder*2+1
        left_slice, middle_slice, right_slice = np.split(dims_array, indices, axis=0)
        for i in range(middle_slice.shape[1]):
            new_middle_slice = scipy.signal.savgol_filter(middle_slice[:, i],  window_length, int_polyorder)
            dims_array[:, i] = np.hstack((left_slice[:, i], new_middle_slice, right_slice[:, i]))
        return dims_array, indices

    @staticmethod
    def amplification(dims_array, indices, factor):
        """
        Function to multiply all given data by a given factor.

        :param dims_array: Original data points.
        :param indices: Begin and end of filter application.
        :param factor: Multiplied on original data.
        :return: Newly shifted data.
        """
        dims_array[indices[0]: indices[1]+1] *= factor
        return dims_array, indices

    @staticmethod
    def level(dims_array, indices, left_values):
        """
        Filter to level the selected data to the value of either the first or last arraypoint. Is used to change
        edit the trajectory of the hand.

        :param dims_array: unchanged data
        :param indices: left and right selection boundary
        :param left_values: boolean to determine if all values get set to the left or right boundary value
        :return: changed data and indices
        """
        if left_values:
            level_values = dims_array[indices[0]]
        else:
            level_values = dims_array[indices[1]]

        dims_array[indices[0]:indices[1]+1] = level_values
        return dims_array, indices

    @staticmethod
    def stop(dims_array, indices):
        """
        function to stop all movement for the selected time. Creates a new array with equal length to selection and
        fills it with values of the beginning of the selection

        :param dims_array: unchanged data
        :param indices: beginning and ending of selection
        :return: changed data
        """
        left_slice, right_slice = np.split(dims_array, [indices[0]], axis=0)
        stopped_slice = np.full((indices[1] - indices[0], dims_array.shape[1]), dims_array[indices[0]])
        dims_array = np.vstack((left_slice, stopped_slice, right_slice))
        return dims_array, indices

    @staticmethod
    def cut(dims_array, indices):
        """
        deletes selected part of the array

        :param dims_array: unchanged array
        :param indices: first and last value that get deleted
        :return: changed array
        """
        left_slice, middle_slice, right_slice = np.split(dims_array, [indices[0]+1, indices[1]])
        dims_array = np.vstack((left_slice, right_slice))
        indices[1] = indices[0]+1
        return dims_array, indices
