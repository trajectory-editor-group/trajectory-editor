import numpy as np
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import *
from PyQt5.QtCore import *

import tredit.config as config
import tredit.data_io as data_io
from tredit.functions import FilterFunctions


class SwitchViewWidget(QWidget):
    """
    Widget for 2D projections of a 3D trajectory. The perspective can then be changed with radio buttons.
    Associated figures are non 3d, non multiplot figures, which means only one graph is visible at once.
    The radio buttons are therefore exclusive: no more than one button per axis can be on at the same time.

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, figure, dimensions):
        """
        Creates a new switch view widget embedding the given figure and creates the buttons needed for the given
        dimensions. If a dim horizontal or vertical is unique (e.g. for dimensions = [(x,t), (y,t), (z,t)] ), the radio
        button is omitted. QButtonGroups are used to preserve the buttons and connect them to click actions.

        :param figure: Figure at the center of the widget.
        :param dimensions: List of all possible graph combinations.
        """

        super().__init__()
        self.figure = figure
        self.dimensions = dimensions
        self.current_dims = dimensions[0]

        self.horizontal_button_group = QButtonGroup()
        self.vertical_button_group = QButtonGroup()

        self.dim_to_button = {is_horizontal: {} for is_horizontal in [False, True]}

        horizontal_dims = sorted(set(dims[0] for dims in dimensions))
        vertical_dims = sorted(set(dims[1] for dims in dimensions))

        layout = QGridLayout()
        if len(vertical_dims) > 1:
            vertical_button_layout = self.create_button_layout(vertical_dims, False)
            layout.addLayout(vertical_button_layout, 0, 0)
            column_counter = 1
        else:
            column_counter = 0

        if len(horizontal_dims) > 1:
            horizontal_button_layout = self.create_button_layout(horizontal_dims, True)
            layout.addLayout(horizontal_button_layout, 1, column_counter)

        layout.addWidget(self.figure.canvas, 0, column_counter)
        if column_counter != 0:
            layout.setColumnStretch(1, 2)
        self.setLayout(layout)

    def create_button_layout(self, dims, is_horizontal):
        """
        Creates the layout containing a group of buttons, either horizontally or vertically aligned.
        The dims are later used as button labels, units are added if needed.

        :param dims: Single dimensions along the orientation
        :param is_horizontal: Orientation of the axes.
        :return: Radiobuttons in a layout.
        """
        if is_horizontal:
            button_layout = QHBoxLayout()
        else:
            button_layout = QVBoxLayout()

        for dim in dims:
            dim_label = dim
            if 'unit' in config.dim_to_property[dim]:
                unit = config.dim_to_property[dim]['unit']
                dim_label += ' [' + unit + ']'
            button = QRadioButton(dim_label, self)
            button.setStyleSheet('font: ' + str(config.qt_fontsize) + 'px; color: black')
            button.setEnabled(False)
            if is_horizontal:
                self.horizontal_button_group.addButton(button)
            else:
                self.vertical_button_group.addButton(button)
            self.dim_to_button[is_horizontal][dim] = button
            button_layout.addWidget(button, 1)
        if is_horizontal:
            button_layout.insertStretch(0, 1)
        return button_layout

    def enable(self):
        """
        Enables all radio buttons of the widget and sets them to match the first dimension combination.
        """
        for button in self.horizontal_button_group.buttons():
            button.setEnabled(True)
        for button in self.vertical_button_group.buttons():
            button.setEnabled(True)

        if len(self.horizontal_button_group.buttons()) != 0:
            self.dim_to_button[True][self.current_dims[0]].setChecked(True)
            self.horizontal_button_group.buttonClicked.connect(lambda b: self.switch_view(b, True))

        if len(self.vertical_button_group.buttons()) != 0:
            self.dim_to_button[False][self.current_dims[1]].setChecked(True)
            self.vertical_button_group.buttonClicked.connect(lambda b: self.switch_view(b, False))

    def switch_view(self, button, is_horizontal, plot=True):
        """
        Switches the view if the button selection of the widget is changed (automatically called).

        :param button: The button that is clicked.
        :param is_horizontal: True if the clicked button is placed along the horizontal axis.
        :param plot: Determines if the plot should change the currently displayed dimensions. Only False on enable to \
        avoid repetition.
        """
        button_dim = button.text().split()[0]
        if is_horizontal:
            self.current_dims = (button_dim, self.current_dims[1])
        else:
            self.current_dims = (self.current_dims[0], button_dim)

        if plot:
            self.figure.set_plot([self.current_dims])


class MultiPlotWidget(QWidget):
    """
    Widget for multiple 2D graphs. These can be turned on and off with their respective checkboxes.
    Associated figures are non 3d, multiplot figures. The checkboxes are solely on the vertical axis, the horizontal
    axis usually displays time. The checkboxes are not exclusive: multiple buttons can be pressed at the same time.

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, figure, primary_dim, secondary_dims):
        """
        Creates a new check box widget embedding the given figure and creates the buttons needed for the given
        secondary dimensions, the primary dim (horizontal axis) is saved to switch displayed graphs.
        If the secondary dim is unique (for single plots), the checkbox is omitted.

        :param figure: Figure at the center of the widget.
        :param primary_dim: Horizontal dim.
        :param secondary_dims: Vertical dims.
        """
        super().__init__()
        self.figure = figure
        self.primary_dim = primary_dim
        self.boxes = []

        layout = QHBoxLayout()
        if len(secondary_dims) > 1:
            layout.addLayout(self.create_checkbox_layout(secondary_dims))
            layout.addWidget(self.figure.canvas, 2)
        else:
            layout.addWidget(self.figure.canvas)
        self.setLayout(layout)

    def create_checkbox_layout(self, dims):
        """
        Creates the layout containing vertically aligned checkboxes.
        The dims are later used as colored checkbox labels, units are added if needed.

        :param dims: Single dimensions for the checkboxes.
        :return: Checkboxes in a layout.
        """
        checkbox_layout = QVBoxLayout()

        for dim in dims:
            dim_label = dim
            if 'unit' in config.dim_to_property[dim]:
                unit = config.dim_to_property[dim]['unit']
                dim_label += ' [' + unit + ']'
            box = QCheckBox(dim_label, self)
            color = config.dim_to_property[dim]['color']
            box.setStyleSheet('font: ' + str(config.qt_fontsize) + 'px; color:' + color)
            box.setCheckable(False)

            self.boxes.append(box)
            checkbox_layout.addWidget(box)

        return checkbox_layout

    def get_checked_boxes(self):
        """
        Returns the currently ticked boxes.

        :return: List of ticked boxes.
        """
        return [(self.primary_dim, box.text().split()[0]) for box in self.boxes if box.isChecked()]

    def check_changed(self):
        """
        Sets the associated figure to display the checked graphs (automatically called).
        """
        self.figure.set_plot(self.get_checked_boxes())

    def enable(self):
        """
        Activates the checkboxes and connects them to check_changed.
        """
        for box in self.boxes:
            box.setCheckable(True)
            box.setChecked(True)
            box.stateChanged.connect(self.check_changed)


class CheckBoxes(QWidget):
    """
    Class for checkboxes that are used in the filter menus.

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, dims):
        """
        Creates colored checkboxes in a horizontal layout, one for each given dim.

        :param dims: Singular dimensions to create checkboxes for.
        """
        super().__init__()
        self.boxes = []

        layout = QHBoxLayout()
        for dim in dims:
            box = QCheckBox(dim, self)
            color = config.dim_to_property[dim]['color']
            box.setStyleSheet('font: ' + str(config.qt_fontsize) + 'px; color:' + color)
            layout.addWidget(box)
            self.boxes.append(box)
        self.setLayout(layout)

    def get_checked_boxes(self):
        """
        Returns checked boxes.

        :return: List of checked dims in the widget.
        """
        return list(box.text().split()[0] for box in self.boxes if box.isChecked())

    def set_all(self, is_checked):
        """
        Sets the status of all checkboxes at once.

        :param is_checked: Boolean representing the new checkbox status.
        """
        for box in self.boxes:
            box.setChecked(is_checked)


class FilterDialog(QWidget):
    """
    Abstract class for a base widget for the menu shown when applying a filter.
    Can contain any number of checkboxes and buttons for 'ok' and 'cancel'.
    Handles saving, loading and replotting.
    """

    def __init__(self, plot):
        """
        Creates a FilterDialog with a window title corresponding to the filter name.
        Uses a QGridlayout and keeps track of current row internally.

        :param plot: For using replot and saving.
        """
        super().__init__()

        self.plot = plot
        self.setWindowTitle(str(self))
        self.setWindowIcon(QIcon('tredit/icon.png'))
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.layout = QGridLayout()
        self.checkboxes_list = []
        self.row_number = 0

    def __str__(self):
        """
        String representation of the filter.
        Abstract method, needs to be overwritten to properly inherit this class.

        :raise: NotImplementedError
        """
        raise NotImplementedError()

    def filter_function(self):
        """
        Uses a function of the Filterfunctions class to edit selected parts of the data.
        Abstract method, needs to be overwritten to properly inherit this class.

        :raise: NotImplementedError
        """
        raise NotImplementedError()

    def statusTip(self):
        """
        Shown in the status bar on the bottom.

        :return: Status Tip of the selected filter.
        """
        return str(self)

    def closeEvent(self, event):
        """
        Differentiates between close events from the exit button / program close and close events
        called directly by the "Ok" / "Cancel" buttons. For the first type cancel procedure is called.

        :param event: Event responsible for the window to close, differentiates event types.
        """
        if event.spontaneous():
            self.on_cancel_click()

    def add_checkboxes(self, group_name, dims):
        """
        Appends a new row of checkboxes with an additional convenience checkbox to trigger all dims at once.

        :param group_name: Text next to the button row.
        :param dims: Singular dimensions to add as buttons.
        """
        label = QLabel()
        label.setText(group_name)
        label.setStyleSheet('font: ' + str(config.qt_fontsize) + 'px; color: black')
        checkboxes = CheckBoxes(dims)
        self.checkboxes_list.append(checkboxes)
        checkall_box = QCheckBox('All', self)
        checkall_box.setStyleSheet('font: ' + str(config.qt_fontsize) + 'px; color: black')
        checkall_box.stateChanged.connect(lambda: checkboxes.set_all(checkall_box.isChecked()))
        checkall_box.setCheckState(2)

        self.row_number += 1
        self.layout.addWidget(label, self.row_number, 0)
        self.layout.addWidget(checkall_box, self.row_number, 1)
        self.layout.addWidget(checkboxes, self.row_number, 2)

    def add_all_checkboxes(self):
        """
        Adds checkboxes for all possible singular dimensions.
        """
        for dim_group_to_dim in config.dimgroups.values():
            for dim_group, dim_to_property in dim_group_to_dim.items():
                dims = list(dim_to_property.keys())
                self.add_checkboxes(dim_group, dims)

    def add_arm_checkboxes(self):
        """
        Adds checkboxes for all arm related singular dimensions.
        """
        for dim_group, dim_to_property in config.dimgroups['Arm'].items():
            dims = list(dim_to_property.keys())
            self.add_checkboxes(dim_group, dims)

    def add_hand_checkboxes(self):
        """
        Adds checkboxes for all hand related singular dimensions.
        """
        for dim_group, dim_to_property in config.dimgroups['Hand'].items():
            dims = list(dim_to_property.keys())
            self.add_checkboxes(dim_group, dims)

    def get_checked_boxes(self):
        """
        Returns all checked boxes in all rows.

        :return: List of checked boxes.
        """
        if not self.checkboxes_list:
            return None
        checked_boxes = []
        for checkboxes in self.checkboxes_list:
            checked_boxes += checkboxes.get_checked_boxes()
        return checked_boxes

    def add_ok_cancel(self):
        """
        Adds the 'ok' and 'cancel' buttons, should be added as the last row.
        """
        button_cancel = QPushButton('Cancel')
        button_cancel.clicked.connect(self.on_cancel_click)
        button_ok = QPushButton('OK')
        button_ok.setDefault(True)
        button_ok.clicked.connect(self.on_ok_click)

        self.row_number += 1
        self.layout.addWidget(button_ok, self.row_number, 0)
        self.layout.addWidget(button_cancel, self.row_number, 2)


    def on_ok_click(self):
        """
        Closes itself and rescales the plot (every figure), if specified in config. Saves once afterwards.
        """
        self.close()
        self.filter_function()
        if config.rescale_on_ok:
            self.plot.replot(self.get_checked_boxes(), draw=False)
            self.plot.scale()
            self.plot.draw()
        else:
            self.plot.replot(self.get_checked_boxes(), draw=True)
        if self.get_checked_boxes() is None or self.get_checked_boxes() != []:
            self.plot.savestate.save()

    def on_cancel_click(self):
        """
        Closes itself.
        """
        self.close()


class SliderFilterDialog(FilterDialog):
    """
    Abstract class for a base widget for the menu shown when applying a filter, inherits FilterDialog.
    Can contain a slider with a corresponding spinbox - a box with parameter value and higher / lower buttons to
    increase / decrease the parameter in single steps, also controllable by mousewheel.
    Can  additionally contain any number of checkboxes and buttons for 'ok' and 'cancel'.
    Handles saving, loading and replotting, uses additional data backups to reset the data before reapplying the filter,
    e.g. switching the slider value from x1 to x2 for a paramter x used in the filter like f(x, data) -> data
    should not result in a combined parameter f(x2, f(x1, data)) instead of f(x2, data).

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, plot):
        """
        Creates a SliderFilterDialog with a window title corresponding to the filter name.
        Uses a QGridlayout and keeps track of current row internally.
        Has additional variables and functions for setting and resetting the slider/spinbox.

        :param plot: For using replot.
        """
        super().__init__(plot)

        self.sliders = []
        self.spinboxes = []
        self.initial_slider_values = []
        self.initial_spinbox_values = []
        self.slider_connections = []
        self.spinbox_connections = []

        self.in_change = False

        plot.selection.connect_selection_change(self.on_selection_change)

    def spinbox_function(self, index, slider_value):
        """
        Slider to spinbox value conversion function dependent on the slider value.
        Abstract method, needs to be overwritten to properly inherit this class.

        :param index: Index of spinbox.
        :param slider_value: Value of connected slider.
        :raise: NotImplementedError
        """
        raise NotImplementedError()

    def slider_function(self, index, spinbox_value):
        """
        Spinbox to slider value conversion function dependent on the spinbox value.
        Abstract method, needs to be overwritten to properly inherit this class.

        :param index: Index of slider.
        :param spinbox_value: Value of connected spinbox.
        :raise: NotImplementedError
        """
        raise NotImplementedError()

    def on_selection_change(self):
        """
        Listens to changes in the selection (possible while in filter menu) and resets the slider and spinbox
        and saves if parameter changes already occurred.
        """
        if self.isVisible() and not self.in_change:
            self.reset_values()
            data_io.create_backup()

    def showEvent(self, event):
        """
        Resets values and creates a data backup.

        :param event: Event responsible for the window to show.
        """
        self.reset_values()
        data_io.create_backup()

    def closeEvent(self, event):
        """
        Restores and deletes the data backup and calls the closeEvent function from FilterDialog.

        :param event: Event responsible for the window to close, differentiates event types.
        """
        super().closeEvent(event)
        data_io.delete_backup()

    def on_ok_click(self):
        """
        Closes itself and undoes all changes done by the filter function.
        """
        data_io.restore_backup()
        super().on_ok_click()

    def on_cancel_click(self):
        """
        Closes itself and undoes all changes done by the filter function.
        """
        data_io.restore_backup()
        self.plot.replot()
        super().on_cancel_click()

    def add_slider(self, initial_slider_value, slider_range, slider_step,
                   initial_spinbox_value, spinbox_range, spinbox_step=0.0001, spinbox_decimals=4, parameter_name='Parameter'):
        """
        Appends two new rows for the spinbox (with parameter name and range) and the slider.
        The spinbox range should be the real parameter range, while the slider can have a wider range to allow greater
        precision (see default conversion functions).

        :param initial_slider_value: Initial and reset value for the slider.
        :param slider_range: Tuple for the range of the slider.
        :param slider_step: Slider step size.
        :param initial_spinbox_value: Initial and reset value for the spinbox.
        :param spinbox_range: Tuple for the range of the spinbox.
        :param spinbox_step: Spinbox step size.
        :param spinbox_decimals: Adjusts how many decimals the spinbox displays.
        :param parameter_name: Name or short description for the parameter next to the spinbox.
        """
        index = len(self.sliders)
        self.sliders.append(QSlider(Qt.Horizontal))
        self.initial_slider_values.append(initial_slider_value)
        self.sliders[index].setFocusPolicy(Qt.StrongFocus)
        self.sliders[index].setRange(*slider_range)
        self.sliders[index].setSingleStep(slider_step)
        self.slider_connections.append(None)

        self.spinboxes.append(QDoubleSpinBox())
        self.initial_spinbox_values.append(initial_spinbox_value)
        self.spinboxes[index].setRange(*spinbox_range)
        self.spinboxes[index].setSingleStep(spinbox_step)
        self.spinboxes[index].setDecimals(spinbox_decimals)
        self.spinbox_connections.append(None)

        self.reset_values()

        spinbox_label = QLabel()
        label_text = parameter_name + ' [' + str(spinbox_range[0]) + '-' + str(spinbox_range[1]) + ']'
        spinbox_label.setText(label_text)
        spinbox_label.setStyleSheet('font: ' + str(config.qt_fontsize) + 'px; color: black')

        self.row_number += 1
        self.layout.addWidget(spinbox_label, self.row_number, 0)
        self.layout.addWidget(self.spinboxes[index], self.row_number, 1)
        self.row_number += 1
        self.layout.addWidget(self.sliders[index], self.row_number, 0, 1, 0)

    def connect_value_changed(self, index):
        """
        Connects the value changed functions of the slider and spinbox.

        :param index: Index of the slider / spinbox.
        """
        self.slider_connections[index] = self.sliders[index].valueChanged.connect(lambda:
                                                                                  self.on_value_change(True, index))
        self.spinbox_connections[index] = self.spinboxes[index].valueChanged.connect(lambda:
                                                                                     self.on_value_change(False, index))

    def disconnect_value_changed(self, index):
        """
        Disconnects the value changed functions of the slider and spinbox.

        :param index: Index of the slider / spinbox.
        """
        self.sliders[index].valueChanged.disconnect(self.slider_connections[index])
        self.spinboxes[index].valueChanged.disconnect(self.spinbox_connections[index])

    def on_value_change(self, is_slider_change, index):
        """
        Reacts to value changes of either the slider or the spinbox by adjusting the other with the conversion function.
        Applies the filter function (with previously restoring the data backup).

        :param is_slider_change: Determines from which widget the change comes.
        :param index: Index of the slider / spinbox.
        """
        self.in_change = True
        self.disconnect_value_changed(index)
        if is_slider_change:
            self.spinboxes[index].setValue(self.spinbox_function(index, self.sliders[index].value()))
        else:
            self.sliders[index].setValue(self.slider_function(index, self.spinboxes[index].value()))
        self.connect_value_changed(index)
        data_io.restore_backup()
        self.filter_function()
        self.plot.replot(self.get_checked_boxes())
        self.in_change = False

    def reset_values(self):
        """
        Resets all slider and spinbox values to their initial values.
        """
        for index in range(len(self.sliders)):
            if self.slider_connections[index] is not None:
                self.disconnect_value_changed(index)
            self.sliders[index].setValue(self.initial_slider_values[index])
            self.spinboxes[index].setValue(self.initial_spinbox_values[index])
            self.connect_value_changed(index)


class LinearFilterDialog(FilterDialog):
    """
    Menu window that appears to set the linear filter. One can select the dimensions with checkboxes.
    Inherits FilterDialog.

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates the menu window with checkboxes for all hand dims, ok button and cancel button.

        :param gui: Given to FilterDialog.
        """
        super().__init__(gui.plot)

        self.add_arm_checkboxes()
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Generation of linear point-to-point trajectories'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Replaces the selection with a linear function connection the start and end value"

    def filter_function(self):
        """
        Creates a slice of the data from the selected checkboxes and edits it with the 'linear' function
        from the FilterFunctions class.
        """
        if not self.get_checked_boxes():
            return
        dims_data = data_io.get_dims_data(self.get_checked_boxes())
        dims_data, indices = FilterFunctions.linear(dims_data, data_io.indices.copy())
        data_io.update(dims_data, self.get_checked_boxes(), indices)


class ExpSmoothingFilterDialog(SliderFilterDialog):
    """
    Creates the window that appears to set the exponential smoothing filter. The smoothing parameter can be
    adjusted with the slider or spinbox. The slider is stretched by factor 100 compared to the spinbox.
    One can select the dimensions with checkboxes.

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates the menu window with checkboxes for all hand dims, spinbox and slider for the smoothing parameter,
        ok button and cancel button.

        :param gui: Given to FilterDialog.
        """
        super().__init__(gui.plot)

        self.add_slider(50, (50, 100), 1, 0.5, (0.5, 1.0))
        self.add_arm_checkboxes()
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Exponential smoothing'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Smoothens the graph in your selection with two symmetric exponential moving averages in both directions"

    def filter_function(self):
        """
        Creates a slice of the data from the selected checkboxes and edits it with the 'exponential_smoothing' function
        from the FilterFunctions class.
        """
        if not self.get_checked_boxes():
            return
        dims_data = data_io.get_dims_data(self.get_checked_boxes())
        dims_data, indices = FilterFunctions.exponential_smoothing(dims_data, data_io.indices.copy(),
                                                                   self.spinboxes[0].value())
        data_io.update(dims_data, self.get_checked_boxes(), indices)

    def spinbox_function(self, index, slider_value):
        """
        Slider to spinbox value conversion function dependent on the slider value.

        :param index: Index of spinbox.
        :param slider_value: Value of connected slider.
        :return: Slider value divided by 100.
        """
        return slider_value / 100

    def slider_function(self, index, spinbox_value):
        """
        Spinbox to slider value conversion function dependent on the spinbox value.

        :param index: Index of slider.
        :param spinbox_value: Value of connected spinbox.
        :return: Int of the spinbox value multiplied by 100.
        """
        return int(spinbox_value * 100)


class SpeedFilterDialog(SliderFilterDialog):
    """
    Creates the window that appears to set the speed filter. The speed factor can be
    adjusted with the slider or spinbox. The spinbox reaches from factor 0.1 to 10 and the slider is centered at 1
    (exponential/logarithmic conversion function). One can select the dimensions with checkboxes.

    :author: Henrik Trommer, Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates the menu window with spinbox and slider for the velocity factor,
        ok button and cancel button.

        :param gui: Used for statusbar access and Given to FilterDialog.
        """
        super().__init__(gui.plot)
        self.gui = gui

        self.add_slider(500, (0, 1000), 1, 1, (0.1, 10), parameter_name='Factor')
        self.add_slider(1, (1, 5), 1, 1, (1, 5), 1, 0, parameter_name='Polynomial Order')
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Change of motion speed'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Resamples the selected part with spline interpolation"

    def filter_function(self):
        """
        Creates a slice of the data with all dims except the time and edits it with the 'speed' function
        from the FilterFunctions class.
        """
        dims = list(data_io.dataframe.columns)
        dims.remove('t')
        dims_data = data_io.get_dims_data(dims)
        try:
            dims_data, indices = FilterFunctions.speed(dims_data, data_io.indices.copy(), self.spinboxes[0].value(),
                                                           self.spinboxes[1].value())
        except TypeError:
            error_message = 'Selection must include at least ' + str(1 + int(self.spinboxes[1].value())) +\
                            ' points (more then the polynomial order).'
            self.gui.show_statusbar_message(error_message, 'red')
            return
        data_io.update(dims_data, dims, indices)

    def spinbox_function(self, index, slider_value):
        """
        Slider to spinbox value conversion function.

        :param index: Index of spinbox.
        :param slider_value: Value of connected slider.
        :return: Normed exponential function with the slider value as exponent, base 10.
        """
        if index == 1:
            return slider_value
        return 0.1 * np.power(10, slider_value / 500)

    def slider_function(self, index, spinbox_value):
        """
        Slider to spinbox value conversion function.

        :param index: Index of slider.
        :param spinbox_value: Value of connected spinbox.
        :return: Int a normed logarithmic function with the spinbox value as anti-logarithm, base 10.
        """
        if index == 1:
            return spinbox_value
        return int((1 + np.log10(spinbox_value)) * 500)


class AmplificationFilterDialog(SliderFilterDialog):
    """
    Creates the window that appears to set the amplification filter. The amplification factor can be
    adjusted with the slider or spinbox. The spinbox reaches from factor 0.1 to 10 and the slider is centered at 1
    (exponential/logarithmic conversion function). One can select the dimensions with checkboxes.

    :author: Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates the menu window with checkboxes for all dims, spinbox and slider for the shift factor,
        ok button and cancel button.

        :param gui: Given to FilterDialog.
        """
        super().__init__(gui.plot)

        self.add_slider(500, (0, 1000), 1, 1, (0.1, 10), parameter_name='Factor')
        self.add_all_checkboxes()
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Trajectory amplification'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Multiplies each point of your selection by a factor resulting an upward/downward shift"

    def filter_function(self):
        """
        Creates a slice of the data from the selected checkboxes and edits it with the 'amplification' function
        from the FilterFunctions class.
        """
        if not self.get_checked_boxes():
            return
        dims_data = data_io.get_dims_data(self.get_checked_boxes())
        dims_data, indices = FilterFunctions.amplification(dims_data, data_io.indices.copy(), self.spinboxes[0].value())
        data_io.update(dims_data, self.get_checked_boxes(), indices)

    def spinbox_function(self, index, slider_value):
        """
        Slider to spinbox value conversion function.

        :param index: Index of spinbox.
        :param slider_value: Value of connected slider.
        :return: Normed exponential function with the slider value as exponent, base 10.
        """
        return 0.1 * np.power(10, slider_value / 500)

    def slider_function(self, index, spinbox_value):
        """
        Slider to spinbox value conversion function.

        :param index: Index of slider.
        :param spinbox_value: Value of connected spinbox.
        :return: Int a normed logarithmic function with the spinbox value as anti-logarithm, base 10.
        """
        return int((1 + np.log10(spinbox_value)) * 500)


class SavitzkyGolayFilterDialog(SliderFilterDialog):
    """
    Creates the window that appears to set the Savitzky-Golay smoothing filter. The polynomial degree can be
    adjusted with the slider or spinbox. One can select the dimensions with checkboxes.

    :author: Fabian Schabram
    """

    def __init__(self, gui):
        """
        Creates the menu window with checkboxes for hand dims, spinbox and slider for the interpolation degree,
        ok button and cancel button.

        :param gui: Given to FilterDialog and used for status bar information.
        """
        super().__init__(gui.plot)
        self.gui = gui

        self.add_slider(5, (5, 10), 1, 5, (5, 10), 1, 0, parameter_name='Interpolation degree')
        self.add_arm_checkboxes()
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Savitzky Golay filter smoothing'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Applies the savitzky golay filter to smoothen the selection"

    def filter_function(self):
        """
        Creates a slice of the data from the selected checkboxes and edits it with the 'savitzky_golay' function
        from the FilterFunctions class.
        """
        if not self.get_checked_boxes():
            return
        dims_data = data_io.get_dims_data(self.get_checked_boxes())
        try:
            dims_data, indices = FilterFunctions.savitzky_golay(dims_data, data_io.indices.copy(),
                                                             self.spinboxes[0].value())
        except ValueError:
            min_points = self.spinboxes[0].value() * 2 + 1
            error_message = 'Selection must include at least 2 * degree + 1 = ' + str(min_points) + ' points.'
            self.gui.show_statusbar_message(error_message, 'red')
            return
        data_io.update(dims_data, self.get_checked_boxes(), indices)

    def spinbox_function(self, index, slider_value):
        """
        Slider to spinbox value conversion function.

        :param index: Index of spinbox.
        :param slider_value: Value of connected slider.
        :return: Slider value (identity function).
        """
        return slider_value

    def slider_function(self, index, spinbox_value):
        """
        Spinbox to slider value conversion function.

        :param index: Index of slider.
        :param spinbox_value: Value of connected spinbox.
        :return: Spinbox value (identity function).
        """
        return spinbox_value


class LevelFilterDialog(FilterDialog):
    """
    Creates the window that appears to set the level filter, used for Hand trajectory generation.
    One can select the dimensions with checkboxes.

    :author: Fabian Schabram, Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates the menu window with checkboxes for finger dims, additional checkbox to choose the left or right border
        as level value, ok button and cancel button.

        :param gui: Given to FilterDialog.
        """
        super().__init__(gui.plot)

        self.level_side_box = QCheckBox('Left side level values', self)
        self.level_side_box.setStyleSheet('font: ' + str(config.qt_fontsize) + 'px; color: black')
        self.level_side_box.setCheckState(2)
        self.row_number += 1
        self.layout.addWidget(self.level_side_box, self.row_number, 0)

        self.add_hand_checkboxes()
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Hand trajectory generation'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Levels to the left or right selection border"

    def filter_function(self):
        """
        Creates a slice of the data from the selected checkboxes and edits it with the 'level' function
        from the FilterFunctions class.
        """
        if not self.get_checked_boxes():
            return
        dims_data = data_io.get_dims_data(self.get_checked_boxes())
        dims_data, indices = FilterFunctions.level(dims_data, data_io.indices.copy(), self.level_side_box.isChecked())
        data_io.update(dims_data, self.get_checked_boxes(), indices)


class CutFilterDialog(FilterDialog):
    """
    Creates the window that appears to set the cut filter.
    One can select the dimensions with checkboxes.

    :author: Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates the menu window with ok button and cancel button.

        :param gui: Given to FilterDialog.
        """
        super().__init__(gui.plot)
        self.layout.setHorizontalSpacing(45)
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Cut'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Cut out selected range"

    def filter_function(self):
        """
        Creates a slice of the data with all dims except the time and edits it with the 'cut' function
        from the FilterFunctions class.
        """
        dims = list(data_io.dataframe.columns)
        dims.remove('t')
        dims_data = data_io.get_dims_data(dims)
        dims_data, indices = FilterFunctions.cut(dims_data, data_io.indices.copy())
        data_io.update(dims_data, dims, indices)


class StopFilterDialog(FilterDialog):
    """
    Creates the window that appears to set the stop filter.
    One can select the dimensions with checkboxes.

    :author: Tim Alexis Körner
    """

    def __init__(self, gui):
        """
        Creates the menu window with ok button and cancel button.

        :param gui: Given to FilterDialog.
        """
        super().__init__(gui.plot)
        self.layout.setHorizontalSpacing(45)
        self.add_ok_cancel()

        self.setLayout(self.layout)

    def __str__(self):
        """
        String representation of the filter.

        :return: Filter name.
        """
        return 'Stop'

    def statusTip(self):
        """
        Information displayed in the statusbar.

        :return: Information string.
        """
        return "Stops trajectory in selected range"

    def filter_function(self):
        """
        Creates a slice of the data with all dims except the time and edits it with the 'stop' function
        from the FilterFunctions class.
        """
        dims = list(data_io.dataframe.columns)
        dims.remove('t')
        dims_data = data_io.get_dims_data(dims)
        dims_data, indices = FilterFunctions.stop(dims_data, data_io.indices.copy())
        data_io.update(dims_data, dims, indices)
