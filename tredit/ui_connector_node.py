#!/usr/bin/python2.7
# license removed for brevity
import rospy
import numpy as np

from std_msgs.msg import Float64MultiArray
from tf.transformations import quaternion_from_euler

import geometry_msgs.msg


'''
def talker():
    pub = rospy.Publisher('chatter', String, queue_size=10)
    rospy.init_node('talker', anonymous=True)
    rate = rospy.Rate(10)  # 10hz

    while not rospy.is_shutdown():
        hello_str = "hello world %s" % rospy.get_time()
        rospy.loginfo(hello_str)
        pub.publish(hello_str)
        rate.sleep()
'''


def publish_trajectory(filepath):

    hand_pub = rospy.Publisher('/schunk_sim_right/ct_position/command', Float64MultiArray, queue_size=1)
    arm_pub = rospy.Publisher('/right_arm/one_task_inverse_kinematics/command', geometry_msgs.msg.Pose, queue_size=1)
    arr = np.genfromtxt(filepath)
    position = arr[:, 0:3]
    # initial_position = position[0, :]
    # position = position - initial_position + np.array([0.5, 0.4, 0.6])

    # euler = arr[:, 3:6]

    '''
    arr = np.zeros((400, 14))
    arr[:, 1] = np.linspace(0, 0, num=400)
    arr[:, 1] = np.linspace(0, 0, num=400)
    arr[:, 2] = np.linspace(0.4, 0.4, num=400) + 0.5
    arr[:, 3] = np.linspace(1, 1, num=400)
    arr[:, 4] = np.linspace(0, 0, num=400)
    arr[:, 5] = np.linspace(0, 0, num=400)
    arr[:, 6] = np.linspace(0, 0, num=400)
    '''
    msg_hand = Float64MultiArray()
    msg_arm = geometry_msgs.msg.Pose()
    for row in arr:
        msg_hand.data = np.array(row)[[6, 7, 8, 6, 9, 10, 11, 12]]*np.pi/180.
        hand_pub.publish(msg_hand)

        msg_arm.position.x = row[0]
        msg_arm.position.y = row[1]
        msg_arm.position.z = row[2] + 0.5

        '''
        quat_array = quaternion_from_euler(row[3], row[4], row[5])
        msg_arm.orientation.x = quat_array[0]
        msg_arm.orientation.y = quat_array[1]
        msg_arm.orientation.z = quat_array[2]
        msg_arm.orientation.w = quat_array[3]
        '''

        msg_arm.orientation.x = row[4]
        msg_arm.orientation.y = row[5]
        msg_arm.orientation.z = row[6]
        msg_arm.orientation.w = row[7]

        msg_arm.orientation.x = 0.707
        msg_arm.orientation.y = 0.707
        msg_arm.orientation.z = 0
        msg_arm.orientation.w = 0

        arm_pub.publish(msg_arm)
        rospy.sleep(0.005)


if __name__ == '__main__':

    rospy.init_node('ui_connector_node')
    print rospy.has_param('/ui_connector_node/filepath')
    filepath = rospy.get_param('/ui_connector_node/filepath', '')
    publish_trajectory(filepath)
