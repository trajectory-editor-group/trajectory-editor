"""
This module contains methods for a own implementation of matplotlib picking, with float indices representing
interpolated data points in between two real, integer indexed data points. This allows smooth selection movement.
The methods can be divided into calculations of float indices, proximity checks and value reconstruction from
close points. A global variable dpi stores dots per inch of the current (virtual) display.

:author: Tim Alexis Körner
"""

import numpy as np
import itertools as itt

import tredit.data_io as data_io

dpi = None


def set_dpi(app):
    """
    Sets the dpi to the registered values in the Qt application. Must be called once in the beginning.

    :param app: Qt application.
    """
    main_screen = app.screens()[0]
    global dpi
    dpi = (main_screen.physicalDotsPerInchX(), main_screen.physicalDotsPerInchY())


def closest_float_index(dims, axes, xclick, yclick=None, previous_index=None, radius_factor=1):
    """
    Calculates the closest float index to the given click position. If there is no float index close enough
    (either out of data range for 1d or not inside a ellipse made from the two picking radii of the axes for 2d),
    none will be returned. The calculation for 2d float indices will start with determining all possible float indices
    (a point can be in range multiple line segments, the float index is a projection on the line segment). If a
    previous index is given, a group of float indices that is closest to the previous index will be used afterwards.
    Finally the float index with the smallest distance out of the remaining float indices will be returned.

    :param dims: Dim tuple which correspond to the data.
    :param axes: Axes on which the closest float index is calculated.
    :param xclick: Horizontal coordinate  click position on the axes.
    :param yclick: Vertical coordinate  click position on the axes.
    :param previous_index: Previously calculated index if existent. Default is None.
    :param radius_factor: Multiplier for the picking radii. Default is 1.
    :return: Float index of the closest position on the data if close enough, otherwise None.
    """
    radii = calculate_radii(axes) * radius_factor
    xdata = data_io.get_dim_data(dims[0], calculate_modulo=True)

    if yclick is None:
        return float_index_1d(xdata, xclick, radii[0])

    ydata = data_io.get_dim_data(dims[1], calculate_modulo=True)
    click_point = np.array([xclick, yclick], dtype=np.float64)
    close_float_indices_and_distance = float_indices_2d(xdata, ydata, click_point, radii)
    if not close_float_indices_and_distance:
        return None
    if previous_index is not None:
        close_float_indices_and_distance = close_to_index(close_float_indices_and_distance, previous_index)
    return min(close_float_indices_and_distance, key=lambda elem: elem[1])[0]


def calculate_radii(axes, radius_in_mm=3):
    """
    Converts the (given) radius in millimeters into two picking radii of axes coordinates.

    :param axes: Axes in which coordinates to convert.
    :param radius_in_mm: Radius in millimeters. Default is 3.
    :return: Picking radii of axes coordinates in a numpy array.
    """
    radius_in_inches = radius_in_mm * 0.0393701
    display_box = [[0, 0], [radius_in_inches * dpi[0], radius_in_inches * dpi[1]]]
    [[left, bottom], [right, top]] = axes.transData.inverted().transform(display_box)
    return np.array([right - left, top - bottom], dtype=np.float64)


def float_index_1d(xdata, xclick, xradius):
    """
    Calculates the float index for 1d (horizontal). Iterates through all points of the 1d data
    until the click position is lower and uses the surrounding indices to linearly interpolate the click position.
    If the click position is outside all data points, with an additional difference greater than the picking radius,
    None will be returned.

    :param xdata: Horizontal data.
    :param xclick: Horizontal click position.
    :param xradius: Horizontal picking radius,
    :return: Float index if the click is close enough to the data or None.
    """
    index = 0
    xvalue = 0
    for index, xvalue in enumerate(xdata):
        if xclick < xvalue:
            break
    if index == 0:
        if xclick + xradius < xvalue:
            return None
        else:
            return index
    if index == xdata.size - 1:
        if xvalue + xradius < xclick:
            return None
        else:
            return index
    prior_xvalue = xdata[index - 1]
    return (xclick - prior_xvalue) / (xvalue - prior_xvalue) + index - 1


def float_indices_2d(xdata, ydata, click_point, radii):
    """
    Calculates all float indices closer than the pick radii to any line segment made from two adjacent data points.
    First all possible data point candidates are filtered with simple checks and afterwards the remaining data
    points and line segments from two adjacent points are tested. The computed float indices and the additional
    distance to the data point or line segment are returned in a list.

    :param xdata: Horizontal data.
    :param ydata: Vertical data.
    :param click_point: Horizontal and vertical click position.
    :param radii: Horizontal and vertical picking radius,
    :return: List of close float indices and their distance.
    """
    filtered_indices = filter_points_2d(xdata, ydata, click_point, radii)
    normed_filtered_data = np.stack((xdata[filtered_indices] / radii[0], ydata[filtered_indices] / radii[1]), axis=-1)
    normed_click_point = click_point / radii
    close_float_indices_and_distance = []

    for data_index, (index1, index2) in enumerate(iterate_pairwise(filtered_indices)):
        normed_point1 = normed_filtered_data[data_index]
        if index1 + 1 == index2:
            normed_point2 = normed_filtered_data[data_index + 1]
            result = check_linepoint2d(normed_point1, normed_point2, normed_click_point)
        else:
            result = check_point2d(normed_point1, normed_click_point)
        if result is not None:
            close_float_indices_and_distance.append((result[0] + index1, result[1]))

    if filtered_indices:
        result = check_point2d(normed_filtered_data[-1], normed_click_point)
        if result is not None:
            close_float_indices_and_distance.append((result[0] + filtered_indices[-1], result[1]))
    return close_float_indices_and_distance


def filter_points_2d(xdata, ydata, click_point, radii):
    """
    Filters data indices by covering them with rectangles, which contain a middle point with its adjacent lines
    and have additional radii sized tolerance along the sides (covering 3 data points).
    This method is an efficient pre selection of possible float indices.
    In order to reduce data access during the iteration, many values are saved and reused.
    Even data sizes need an additional check.

    :param xdata: Horizontal data.
    :param ydata: Vertical data.
    :param click_point: 2d click position.
    :param radii: Horizontal and vertical picking radius.
    :return: Filtered possible float indices.
    """
    filtered_indices = []
    xradius, yradius = radii
    xclick, yclick = click_point

    def rectangle_filter(xmin, xmax, ymin, ymax, start_index, medium_index, stop_index):
        """
        Checks if the click is inside the rectangle covering the given indices with the borders at the given
        horizontal and vertical values. If so appends the indices to a nonlocal list of filtered indices.

        :param xmin: Left border of the rectangle.
        :param xmax: Right border of the rectangle.
        :param ymin: Bottom border of the rectangle.
        :param ymax: Top border of the rectangle.
        :param start_index: First covered index.
        :param medium_index: Second covered index.
        :param stop_index: Third covered index.
        """
        if xmin - xradius < xclick and ymin - yradius < yclick and \
                xmax + xradius > xclick and ymax + yradius > yclick:
            nonlocal filtered_indices
            if filtered_indices == [] or filtered_indices[-1] != start_index:
                filtered_indices.append(start_index)
            filtered_indices.append(medium_index)
            filtered_indices.append(stop_index)

    def min_max(a, b, c):
        """
        Finds the minimum and maximum out of 3 values.

        :param a: One out of three values.
        :param b: One out of three values.
        :param c: One out of three values.
        :return: Minimum and maximum as a tuple.
        """
        if a < b:
            if b < c:
                return a, c
            else:
                if a < c:
                    return a, b
                else:
                    return c, b
        else:
            if a < c:
                return b, c
            else:
                if b < c:
                    return b, a
                else:
                    return c, a

    start_index = 0
    x0 = xdata[0]
    y0 = ydata[0]
    for medium_index in range(1, xdata.size - 1, 2):
        stop_index = medium_index + 1
        x1 = xdata[medium_index]
        y1 = ydata[medium_index]
        x2 = xdata[stop_index]
        y2 = ydata[stop_index]
        rectangle_filter(*min_max(x0, x1, x2), *min_max(y0, y1, y2), start_index, medium_index, stop_index)
        start_index = stop_index
        x0 = x2
        y0 = y2

    if xdata.size % 2 == 0:
        medium_index = xdata.size - 2
        start_index = medium_index - 1
        stop_index = medium_index + 1
        x0 = xdata[start_index]
        y0 = ydata[start_index]
        x1 = xdata[medium_index]
        y1 = ydata[medium_index]
        x2 = xdata[stop_index]
        y2 = ydata[stop_index]
        rectangle_filter(*min_max(x0, x1, x2), *min_max(y0, y1, y2), start_index, medium_index, stop_index)
    return filtered_indices


def iterate_pairwise(iterable):
    """
    Creates pair iterator for example iterator([1,2,3]) -> iterator([(1,2), (2,3)]).

    :param iterable: Iterator to bundle pairwise.
    :return: Pairwise iterator.
    """
    a, b = itt.tee(iterable)
    next(b, None)
    return zip(a, b)


def check_linepoint2d(normed_point1, normed_point2, normed_click_point):
    """
    Builds a rectangle which encloses and has one side parallel to the line segment between the given
    normed click points. The rectangle is precisely big enough to contain all circles with radius 1
    around every point on the line segment, except points behind the second normed point (redundant).
    Checks if the rectangle contains the normed click point and calculates the parallel distance along the line
    segment and perpendicular distance to the line segment, measured from the first point.

    :param normed_point1: First pick radii normed point of a line segment.
    :param normed_point2: Second pick radii normed point of a line segment.
    :param normed_click_point: Radii normed click point.
    :return: Parallel and perpendicular distance to the first point, if close enough. None otherwise.
    """
    p1p2_vector = normed_point2 - normed_point1
    p1p2_normsq = p1p2_vector @ p1p2_vector
    if p1p2_normsq == 0:
        return None
    p1cp_vector = normed_click_point - normed_point1
    parallel_dist = (p1p2_vector @ p1cp_vector) / p1p2_normsq

    if parallel_dist < 0:
        return check_point2d(normed_point1, normed_click_point)
    elif parallel_dist < 1:
        p1p2_perp_vector = np.array([p1p2_vector[1], -p1p2_vector[0]], dtype=np.float64)
        normed_p1p2_perp_vector = p1p2_perp_vector / np.sqrt(p1p2_normsq)
        perp_dist = abs(normed_p1p2_perp_vector @ p1cp_vector)
        if perp_dist < 1:
            return parallel_dist, perp_dist


def check_point2d(normed_point, normed_click_point):
    """
    Checks if two picker radii normed points have a distance less then 1, in which case it
    returns the parallel distance (always 0) and the perpendicular distance (point distance).

    :param normed_point: One out of two pick radii normed points.
    :param normed_click_point: One out of two pick radii normed points.
    :return: 0 and distance if distance is smaller than one, None otherwise.
    """
    p1cp_vector = normed_click_point - normed_point
    p1cp_normsq = p1cp_vector @ p1cp_vector
    if p1cp_normsq < 1:
        return 0, p1cp_normsq ** 0.5


def close_to_index(close_float_indices_and_distance, index):
    """
    Bundles float indices into group with a maximum index difference between each index
    and returns the group with the closest median float index to the given index.

    :param close_float_indices_and_distance:
    :param index: Index for which the closest group is determined.
    :return: Float indices that are closest to the index and their distance
    """
    index_groups = []
    last_elem = close_float_indices_and_distance[0]
    group = [last_elem]
    for i in range(1, len(close_float_indices_and_distance)):
        current_elem = close_float_indices_and_distance[i]
        if abs(last_elem[0] - current_elem[0]) < 5:
            group.append(current_elem)
        else:
            index_groups.append(group)
            group = [current_elem]
        last_elem = current_elem
    index_groups.append(group)
    return min(index_groups, key=lambda index_group: abs(index_group[int(len(index_group)/2)][0] - index))


def is_close1d(axes, x1, x2):
    """
    Checks if the given 1d positions are closer then the horizontal pick radius of the axes.

    :param axes: Axes for pick radius calculation.
    :param x1: One of two positions.
    :param x2: One of two positions.
    :return: Boolean representing if the positions are close.
    """
    return abs(x1 - x2) < calculate_radii(axes)[0]


def is_close2d(axes, point1, point2):
    """
    Checks if the given 2d positions are inside the ellipse created by the pick radii of the axes.

    :param axes: Axes for pick radii calculation.
    :param point1: One of two positions.
    :param point2: One of two positions.
    :return: Boolean representing if the positions are close.
    """
    radii = calculate_radii(axes)
    return (((point1[0] - point2[0])/radii[0]) ** 2 + ((point1[1] - point2[1])/radii[1]) ** 2) < 1


def value_at_index(float_index, dim):
    """
    Calculates the value of the float index in the data corresponding to a single dim.
    This means the value is interpolated (linear between the surrounding integer indices.

    :param float_index: Non integer position.
    :param dim: Dim corresponding to data.
    :return: Value at float index.
    """
    prior_index = int(float_index)
    prior_value = data_io.get_singular_value(dim, prior_index, calculate_modulo=True)
    if prior_index == float_index:
        return prior_value
    next_value = data_io.get_singular_value(dim, prior_index + 1, calculate_modulo=True)
    return (float_index - prior_index) * (next_value - prior_value) + prior_value


def point_at_index(float_index, dims):
    """
    Creates a list of values at the float index of the dim data for each dims.

    :param float_index: Non integer position.
    :param dims: Tuple of dim corresponding to data.
    :return: List of values at the float index for each dim.
    """
    return [value_at_index(float_index, dim) for dim in dims]
