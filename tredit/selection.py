from matplotlib.lines import Line2D
from matplotlib.patches import Rectangle
from mpl_toolkits.mplot3d.art3d import Line3D

import tredit.data_io as data_io
import tredit.pickercalculations as pickcalc


class Selection:
    """
    Contains all selection markers of all figures (left boundaries, right boundaries and spans).
    Processes left button canvas clicks and drags for boundary changes and decides which selection markers
    should be visible or highlighted. Keeps track on the exact (float index) position of each boundary,
    which translate to the ordered and rounded indices of data.
    Methods include global selection changes (position and appearance), adding figures,
    adding and removing selection markers, handling mouse input events.

    :author: Tim Alexis Körner
    """

    def __init__(self, plot, gui):
        """
        Initialises the selection with a given draw function for selection updates, and empty lists to store and keep
        track of the selection markers.

        :param plot: Used for drawing.
        :param gui: Used for checking if a filter widget is active.
        """
        self.plot = plot
        self.gui = gui
        self.boundary_positions = [None, None]
        self.first_boundaries = []
        self.second_boundaries = []
        self.spans = []
        self.pressed_boundary = None
        self.on_selection_change_functions = []
        self.active_filter = False

    def connect_selection_change(self, function):
        """
        Stores a function which should be called whenever the selection changes.

        :param function: Function to be called on selection change.
        """
        self.on_selection_change_functions.append(function)

    def on_selection_change(self):
        """
        Iterates and calls all functions which should be called on selection change.
        """
        for function in self.on_selection_change_functions:
            function()

    def update_position(self, float_index1, float_index2, draw=True):
        """
        Determines the appearance (visible, highlighted) and updates the position for all selection markers,
        dependent on the given float indices. Draws if not told otherwise.

        :param float_index1: Position of the first boundary in all selections.
        :param float_index2: Position of the second boundary in all selections.
        :param draw: Boolean representing if a draw function should be called after updates. Default is True.
        """
        if float_index1 is None and float_index2 is None:
            for boundary in self.first_boundaries:
                boundary.set_visible(False)
            for boundary in self.second_boundaries:
                boundary.set_visible(False)
            for span in self.spans:
                span.set_visible(False)

        elif float_index1 is None or float_index2 is None:
            if float_index1 is None:
                float_index2 = None
            if float_index2 is None:
                for boundary in self.first_boundaries:
                    boundary.set_visible(True)
                for boundary in self.second_boundaries:
                    boundary.set_visible(False)
                for span in self.spans:
                    span.set_visible(False)

        else:
            for boundary in self.first_boundaries:
                boundary.set_visible(True)
            for boundary in self.second_boundaries:
                boundary.set_visible(True)
            for span in self.spans:
                span.set_visible(True)
            self.update_span_position(float_index1, float_index2)

        self.update_boundary_position(0, float_index1)
        self.update_boundary_position(1, float_index2)

        if draw:
            self.plot.quickdraw()

    def update_round_position(self, float_index1, float_index2, draw=True):
        """
        Rounds the given float indices to an integer interval with a minimum width of one.
        Calls update position with the rounded float indices. Draws if not told otherwise.

        :param float_index1: Position of the first boundary in all selections.
        :param float_index2: Position of the second boundary in all selections.
        :param draw: Boolean representing if a draw function should be called after updates. Default is True.
        """
        if float_index1 is not None and float_index2 is not None:
            first_index = round(float_index1)
            second_index = round(float_index2)
            if first_index == second_index:
                if abs(first_index - float_index1) < abs(second_index - float_index2):
                    if float_index1 < float_index2:
                        second_index = first_index + 1
                    else:
                        second_index = first_index - 1
                else:
                    if float_index2 < float_index1:
                        first_index = second_index + 1
                    else:
                        first_index = second_index - 1
        else:
            first_index = float_index1
            second_index = float_index2
        self.update_position(first_index, second_index, draw)

    def select_everything(self, draw=True):
        """
        Changes the selection to span over the whole data. Draws if not told otherwise.

        :param draw: Boolean representing if a draw function should be called after the update. Default is True.
        """
        self.update_position(0, len(data_io.dataframe.index) - 1, draw)

    def clear_selection(self, draw=True):
        """
        Clears the selection (no visible selection markers), if currently no data backup exists (otherwise data
        manipulation on no selection becomes possible while filter functions are active). Draws if not told otherwise.

        :param draw: Boolean representing if a draw function should be called after the update. Default is True.
        """
        if not self.gui.has_open_filter():
            self.update_position(None, None, draw)

    def recalculate_position(self, draw=True):
        """
        Sets the ordered float indices of data as the current boundary positions. Draws if not told otherwise.

        :param draw: Boolean representing if a draw function should be called after the update. Default is True.
        """
        self.update_position(*data_io.float_indices, draw)

    def add_figure(self, figure, dims):
        """
        Adds a figure by creating selection markers corresponding to the figure type. 3 dimensional figures
        get a non interactive pair of 3d points and a 3d line segment in between. 2 dimensional multiplots (time as
        horizontal axis) get a pair of interactive vertical lines which bound a rectangle. 2 dimensional non multiplots
        get interactive 2d points with a 2d line segment in between.
        The given dims are handed to the new selection markers, which are stored in the corresponding class lists.

        :param figure: Figure to create the selection markers for.
        :param dims: All possible dimension tuples of the figure.
        :return: Selection marker group: Tuple containing a span, first and second boundary, in that order.
        """
        if figure.is_3d:
            first_boundary = figure.axes[0].add_artist(VisiblePoint3D(dims))
            second_boundary = figure.axes[0].add_artist(VisiblePoint3D(dims))
            span = figure.axes[0].add_artist(AdjustingLineSegment3D(dims))
        else:
            if figure.is_multiplot:
                first_boundary = figure.axes[0].add_artist(DraggableLine(dims))
                second_boundary = figure.axes[0].add_artist(DraggableLine(dims))
                span = figure.axes[0].add_artist(AdjustingRect(dims))
            else:
                first_boundary = figure.axes[0].add_artist(DraggablePoint(dims))
                second_boundary = figure.axes[0].add_artist(DraggablePoint(dims))
                span = figure.axes[0].add_artist(AdjustingLineSegment(dims))

        self.first_boundaries.append(first_boundary)
        self.second_boundaries.append(second_boundary)
        self.spans.append(span)
        return span, first_boundary, second_boundary

    def add_markers(self, selection_marker_group, axes):
        """
        Adds the given markers to the given axes and class lists. Afterwards the selections markers position
        and visibility are updated to match the current selection.

        :param selection_marker_group: Tuple containing a span, first and second boundary, in that order.
        :param axes: Axes of a figure to add the selection markers on, if not already added.
        """
        span, first_boundary, second_boundary = selection_marker_group
        if span in self.spans:
            return
        if first_boundary.axes is None:
            axes.add_artist(first_boundary)
        if second_boundary.axes is None:
            axes.add_artist(second_boundary)
        if span.axes is None:
            axes.add_artist(span)
        self.first_boundaries.append(first_boundary)
        self.second_boundaries.append(second_boundary)
        self.spans.append(span)

        if self.boundary_positions[0] is not None:
            first_boundary.update_position(self.boundary_positions[0])
            first_boundary.set_visible(True)
        else:
            first_boundary.set_visible(False)

        if self.boundary_positions[1] is not None:
            second_boundary.update_position(self.boundary_positions[1])
            second_boundary.set_visible(True)
        else:
            second_boundary.set_visible(False)

        if None not in self.boundary_positions:
            span.update_position(*sorted(self.boundary_positions))
            span.set_visible(True)
        else:
            span.set_visible(False)

    def remove_markers(self, selection_marker_group):
        """
        Removes the selection markers of the given group from the axes of its figure. Also removes the selection markers
        from the class lists, if stored.

        :param selection_marker_group: Tuple containing a span, first and second boundary, in that order.
        """
        span, first_boundary, second_boundary = selection_marker_group
        for artist in selection_marker_group:
            if artist.axes is not None:
                artist.remove()
        if span not in self.spans:
            return
        self.first_boundaries.remove(first_boundary)
        self.second_boundaries.remove(second_boundary)
        self.spans.remove(span)

    def connect(self, canvas):
        """
        Connects the canvas button press and release events and the mouse motion notify event to the
        corresponding input process functions.

        :param canvas: Canvas where the events are triggered in.
        """
        canvas.mpl_connect('button_press_event', self.on_press)
        canvas.mpl_connect('button_release_event', lambda event: self.on_release())
        canvas.mpl_connect('motion_notify_event', self.on_motion)

    def on_press(self, event):
        """
        Handles press events of single left button clicks if the click was recorded inside an axes.
        Does not handle new clicks while the old one is still pressed.
        Finds out if and which selection boundary was pressed in the triggered canvas and refers
        to process_event_in_boundary to update the selection.

        :param event: Mouse press event with button information and canvas.
        """
        if event.button != 1 or event.inaxes is None or event.dblclick or self.pressed_boundary is not None:
            return

        for selection_marker_group in event.canvas.figure.selection_marker_groups:
            _, first_boundary, second_boundary = selection_marker_group
            if first_boundary not in self.first_boundaries:
                continue
            if not self.process_event_in_boundary(event, first_boundary, second_boundary, 0):
                self.process_event_in_boundary(event, second_boundary, first_boundary, 1)

    def process_event_in_boundary(self, event, boundary, other_boundary, boundary_number):
        """
        Checks if the given boundary contains the event. If it does or the boundary is currently invisible, the boundary
        and its number are stored as the pressed boundary. The given boundary get visible and highlighted.
        If both boundaries are now visible the span gets visible.
        The span position also changes if needed. If any visible changes occurred, the draw function will be called.

        :param event: Mouse press with position information.
        :param boundary: Boundary may contain the event.
        :param other_boundary: Other boundary which is not currently checked.
        :param boundary_number: 0 or 1 if the boundary is the first or second one.
        :return: Boolean representing if the boundary contains the event.
        """
        if not boundary.get_visible() or boundary.contains(event):
            float_index = boundary.calculate_float_index(event)
            if float_index is not None:
                if boundary_number == 0:
                    boundaries = self.first_boundaries
                else:
                    boundaries = self.second_boundaries

                self.pressed_boundary = (boundary, boundary_number)
                self.update_boundary_position(boundary_number, float_index)
                for boundary in boundaries:
                    boundary.highlight()

                if not boundary.get_visible():
                    for boundary in boundaries:
                        boundary.set_visible(True)
                    if other_boundary.get_visible():
                        for span in self.spans:
                            span.set_visible(True)

                if other_boundary.get_visible():
                    self.update_span_position(*self.boundary_positions)
                self.plot.quickdraw()
                return True
        return False

    def on_motion(self, event):
        """
        If the motion is recorded inside an axes while the mouse button is still pressed,
        the float index position of the event is calculated.
        If the motion is inside the data range, the pressed boundary and span positions get updated and drawn.

        :param event: Mouse motion event with position information.
        """
        if self.pressed_boundary is None or event.inaxes is None:
            return
        boundary, boundary_number = self.pressed_boundary
        previous_index = self.boundary_positions[boundary_number]
        float_index = boundary.calculate_float_index(event, previous_index=previous_index, radius_factor=2)
        if float_index is not None:
            self.update_boundary_position(boundary_number, float_index)
            self.update_span_position(*self.boundary_positions)
            self.plot.quickdraw()

    def on_release(self):
        """
        If a boundary was selected, the positions of boundaries and spans get rounded and updated, and the boundaries
        get de-highlighted.
        """
        if self.pressed_boundary is None:
            return
        self.pressed_boundary = None
        self.update_round_position(*self.boundary_positions)
        for boundaries in [self.first_boundaries, self.second_boundaries]:
            for boundary in boundaries:
                boundary.de_highlight()
        self.plot.quickdraw()

    def update_boundary_position(self, boundary_number, float_index):
        """
        Updates all first or second boundaries determined by the given number (0 or 1) with the given float index.

        :param boundary_number: 0 or 1, corresponding to all first or all second boundaries.
        :param float_index: New position of the boundaries.
        """
        self.boundary_positions[boundary_number] = float_index
        data_io.update_indices(self.boundary_positions)
        self.on_selection_change()

        if float_index is not None:
            if boundary_number == 0:
                boundaries = self.first_boundaries
            else:
                boundaries = self.second_boundaries
            for boundary in boundaries:
                boundary.update_position(float_index)

    def update_span_position(self, float_index1, float_index2):
        """
        Updates all spans with the given float indices, after sorting them.

        :param float_index1: One of two positions.
        :param float_index2: One of two positions.
        """
        if float_index1 is None or float_index2 is None:
            return
        if float_index1 < float_index2:
            min_float_index = float_index1
            max_float_index = float_index2
        else:
            min_float_index = float_index2
            max_float_index = float_index1
        for span in self.spans:
            span.update_position(min_float_index, max_float_index)


class DraggableLine(Line2D):
    """
    A interactively draggable, vertical line that spans across the complete canvas at a horizontal position.
    It is adjusted to specific selection operations with a new contains method. Inherits Line2D.

    :author: Tim Alexis Körner
    """

    def __init__(self, dims):
        """
        Initializes the line and sets it invisible.

        :param dims: A horizontal and vertical dim, in that order.
        """
        super().__init__([0, 0], [0, 0], linewidth=2, linestyle='solid', color='steelblue')
        self.set_visible(False)
        self.dims = dims

    def contains(self, mouse_event):
        """
        Uses the is close 1d function of the picker module with the horizontal positions of the line and event.

        :param mouse_event: Mouse event with position data,
        :return: Boolean representing if the event is close enough to the line.
        """
        return pickcalc.is_close1d(self.axes, self.get_xdata(), mouse_event.xdata)

    def calculate_float_index(self, event, **kwargs):
        """
        Uses a function of the picker module to calculate the float index on the data of the dims.

        :param event: Mouse event with position data.
        :param kwargs: Additional keyword arguments / dictionary handed to the picker function.
        :return: Float index on the data of the dims.
        """
        return pickcalc.closest_float_index(self.dims, self.axes, event.xdata, **kwargs)

    def update_position(self, float_index):
        """
        Updates the horizontal position of the line to the horizontal data position of the dims at the float index.

        :param float_index: Position on the data.
        """
        xpos = pickcalc.point_at_index(float_index, self.dims)[0]
        self.set_xdata(xpos)

    def highlight(self):
        """
        Highlights the line.
        """
        self.set_color('indianred')

    def de_highlight(self):
        """
        De-highlights the line.
        """
        self.set_color('steelblue')

    def draw_artist(self):
        """
        Sets the vertical range of the line to span the whole canvas.
        Draws itself on the axes.
        """
        self.set_ydata(self.axes.get_ylim())
        self.axes.draw_artist(self)


class DraggablePoint(Line2D):
    """
    A interactively draggable, point as a line with length zero and round marker at its singular data point.
    It is adjusted to specific selection operations with a new contains method. Inherits Line2D.

    :author: Tim Alexis Körner
    """

    def __init__(self, dims):
        """
        Initializes the point/line with length zero and a round marker and sets it invisible.

        :param dims: A horizontal and vertical dim, in that order.
        """
        super().__init__([0], [0], linewidth=0, linestyle='None', markersize=15, marker='.')
        self.de_highlight()
        self.set_visible(False)
        self.dims = dims

    def contains(self, mouse_event):
        """
        Uses the is close 2d function of the picker module
        with the horizontal positions of the point/line and event.

        :param mouse_event: Mouse event with position data,
        :return: Boolean representing if the event is close enough to the line.
        """
        click_point = (mouse_event.xdata, mouse_event.ydata)
        return pickcalc.is_close2d(self.axes, self.get_xydata()[0], click_point)

    def calculate_float_index(self, event, **kwargs):
        """
        Uses a function of the picker module to calculate the float index on the data of the dims.

        :param event: Mouse event with position data.
        :param kwargs: Additional keyword arguments / dictionary handed to the picker function.
        :return: Float index on the data of the dims.
        """
        return pickcalc.closest_float_index(self.dims, self.axes, event.xdata, event.ydata, **kwargs)

    def update_position(self, float_index):
        """
        Updates the position of the point/line to the data position of the dims at the float index.

        :param float_index: Position on the data.
        """
        xpos, ypos = pickcalc.point_at_index(float_index, self.dims)
        self.set_xdata(xpos)
        self.set_ydata(ypos)

    def highlight(self):
        """
        Highlights the line.
        """
        self.set_markerfacecolor('indianred')
        self.set_markeredgecolor('indianred')

    def de_highlight(self):
        """
        De-highlights the line.
        """
        self.set_markerfacecolor('steelblue')
        self.set_markeredgecolor('steelblue')

    def draw_artist(self):
        """
        Draws itself on the axes.
        """
        self.axes.draw_artist(self)


class VisiblePoint3D(Line3D):
    """
    A non interactively draggable, point as a line with length zero and round marker at its singular data point.
    It is adjusted to specific selection operations with a new contains method. Inherits Line3D.

    :author: Tim Alexis Körner
    """

    def __init__(self, dims):
        """
        Initializes the point/line with length zero and a round marker and sets it invisible.

        :param dims: A three dims of the data on the figure.
        """
        super().__init__([0], [0], [0], linewidth=0, linestyle='None', markersize=15, marker='.')
        self.de_highlight()
        self.set_visible(False)
        self.dims = dims

    def contains(self, mouse_event):
        """
        Not implemented for 3d points.

        :raise: NotImplementedError
        """
        raise NotImplementedError()

    def calculate_float_index(self, event, previous_index=None, radius_factor=1):
        """
        Not implemented for 3d points.

        :raise: NotImplementedError
        """
        raise NotImplementedError()

    def update_position(self, float_index):
        """
        Updates the position of the point/line to the data position of the dims at the float index.

        :param float_index: Position on the data.
        """
        xpos, ypos, zpos = pickcalc.point_at_index(float_index, self.dims)
        self.set_data_3d(xpos, ypos, zpos)

    def highlight(self):
        """
        Highlights the line.
        """
        self.set_markerfacecolor('indianred')
        self.set_markeredgecolor('indianred')

    def de_highlight(self):
        """
        De-highlights the line.
        """
        self.set_markerfacecolor('steelblue')
        self.set_markeredgecolor('steelblue')

    def draw_artist(self):
        """
        Draws itself on the axes.
        """
        self.axes.draw_artist(self)


class AdjustingRect(Rectangle):
    """
    A adjusting rectangle that spans across the complete canvas between two vertical positions.
    It is adjusted to specific selection operations. Inherits Rectangle.

    :author: Tim Alexis Körner
    """

    def __init__(self, dims):
        """
        Initializes the rectangle and sets it invisible.

        :param dims: A horizontal and vertical dim, in that order.
        """
        super().__init__((0, 0), 0, 0, linewidth=0, fill=True, color='gray', alpha=0.5)
        self.set_visible(False)
        self.dims = dims

    def update_position(self, min_float_index, max_float_index):
        """
        Updates the positions of the rectangle to the data span of the dims between the float indices.

        :param min_float_index: First position on the data.
        :param max_float_index: Last position on the data.
        """
        min_x = pickcalc.point_at_index(min_float_index, self.dims)[0]
        max_x = pickcalc.point_at_index(max_float_index, self.dims)[0]
        self.set_x(min_x)
        self.set_width(max_x - min_x)

    def draw_artist(self):
        """
        Sets the vertical range of the rectangle to span the whole canvas.
        Draws itself on the axes.
        """
        min_y, max_y = self.axes.get_ylim()
        self.set_y(min_y)
        self.set_height(max_y - min_y)
        self.axes.draw_artist(self)


class AdjustingLineSegment(Line2D):
    """
    A adjusting line segment that spans across the complete canvas between two positions.
    It is adjusted to specific selection operations. Inherits Line2D.

    :author: Tim Alexis Körner
    """

    def __init__(self, dims):
        """
        Initializes the line segment and sets it invisible.

        :param dims: A horizontal and vertical dim, in that order.
        """
        super().__init__([0], [0], linewidth=5, linestyle='solid', color='gray', alpha=0.5)
        self.set_visible(False)
        self.dims = dims

    def update_position(self, min_float_index, max_float_index):
        """
        Updates the points of the line segment to the data span of the dims between the float indices.
        This means that the border points are interpolated float index points
        and all points in between are real data points.

        :param min_float_index: First position on the data.
        :param max_float_index: Last position on the data.
        """
        min_index = int(min_float_index)
        max_index = int(max_float_index) + 1
        if max_index == len(data_io.dataframe.index):
            max_index -= 1

        dims_data = []
        for dim in self.dims:
            dim_data = data_io.get_dim_data(dim, calculate_modulo=True)[min_index: max_index + 1]
            dims_data.append(dim_data)

        dims_data[0][0], dims_data[1][0] = pickcalc.point_at_index(min_float_index, self.dims)
        dims_data[0][-1], dims_data[1][-1] = pickcalc.point_at_index(max_float_index, self.dims)
        self.set_xdata(dims_data[0])
        self.set_ydata(dims_data[1])

    def draw_artist(self):
        """
        Draws itself on the axes.
        """
        self.axes.draw_artist(self)


class AdjustingLineSegment3D(Line3D):
    """
    A adjusting line segment that spans across the complete canvas between two positions.
    It is adjusted to specific selection operations. Inherits Line3D.

    :author: Tim Alexis Körner
    """

    def __init__(self, dims):
        """
        Initializes the line segment and sets it invisible.

        :param dims: A horizontal and vertical dim, in that order.
        """
        super().__init__([0], [0], [0], linewidth=5, linestyle='solid', color='gray', alpha=0.5)
        self.set_visible(False)
        self.dims = dims

    def update_position(self, min_float_index, max_float_index):
        """
        Updates the points of the line segment to the data span of the dims between the float indices.
        This means that the border points are interpolated float index points
        and all points in between are real data points.

        :param min_float_index: First position on the data.
        :param max_float_index: Last position on the data.
        """
        min_index = int(min_float_index)
        max_index = int(max_float_index) + 1
        if max_index == len(data_io.dataframe.index):
            max_index -= 1

        dims_data = []
        for dim in self.dims:
            dim_data = data_io.get_dim_data(dim, calculate_modulo=True)[min_index: max_index + 1]
            dims_data.append(dim_data)

        dims_data[0][0], dims_data[1][0], dims_data[2][0] = pickcalc.point_at_index(min_float_index, self.dims)
        dims_data[0][-1], dims_data[1][-1], dims_data[2][-1] = pickcalc.point_at_index(max_float_index, self.dims)
        self.set_data_3d(*dims_data)

    def draw_artist(self):
        """
        Draws itself on the axes.
        """
        self.axes.draw_artist(self)
