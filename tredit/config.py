"""
Module containing configuration variables, Please refer to the manual for further details.

:author: Tim Alexis Körner
"""

from collections import OrderedDict
from matplotlib.colors import to_hex

from tredit.functions import DimFunctions as DF

"""
Adjustable parameters:
- show_exit_window: show the exit window upon trying to close the gui
- split_output_data: creates three files instead of one on data output: complete, hand, finger
- statusbar_timer: display time of status tips in the statusbar in ms
- simulation_path: path to the simulator start script with simulation file path handed as argument
- show_modulo: display modulo data continuously
- rescale_on_ok: autoscaling the plot after using a filter
- fontsize: Qt and Matplotlib
- default_color: for plots (matplotlib color)
- max_save_count: the maximum amount of saved data states (using undo and redo)
- frequency: data sampling frequency
"""
show_exit_window = True
split_output_data = True
statusbar_timer = 2000
simulation_path = '/home/okaya/Desktop/trommer/trajectory-editor/tredit/run_rosnode.sh'
show_modulo = False
rescale_on_ok = True
qt_fontsize = 12
mpl_fontsize = int(qt_fontsize * 0.75)
default_color = 'black'
max_save_count = 5
frequency = 50
DF.frequency = frequency

"""
Adjustable dimgroups, defining all used single dimensions (dims) and their properties,
all properties (except modulo and unit) must be for matplotlib.lines.Line2D.

dimgroups = {arm_part: dim_group_to_dim},
dim_group_to_dim = {dim_group: dim_of_group_to_property}
dim_of_group_to_property = {dim: property_to_value}
property_to_value = {property: value}
"""
dimgroups = OrderedDict([
    ('Arm', OrderedDict([
        ('Position', OrderedDict([
            ('x', {'color': 'red', 'unit': 'm'}),
            ('y', {'color': 'blue', 'unit': 'm'}),
            ('z', {'color': 'green', 'unit': 'm'})
        ])),
        ('Orientation', OrderedDict([
            ('α', {'color': 'red', 'modulo': 360, 'unit': '°'}),
            ('β', {'color': 'blue', 'modulo': 360, 'unit': '°'}),
            ('γ', {'color': 'green', 'modulo': 360, 'unit': '°'})
        ])),
    ])),
    ('Hand', OrderedDict([
        ('Spread angle', OrderedDict([
            ('δ', {'color': 'dimgray', 'unit': '°'})
        ])),
        ('Left joints', OrderedDict([
            ('l1', {'color': 'darkorange', 'unit': '°'}),
            ('l2', {'color': 'orangered', 'unit': '°'})
        ])),
        ('Middle joints', OrderedDict([
            ('m1', {'color': 'yellowgreen', 'unit': '°'}),
            ('m2', {'color': 'olive', 'unit': '°'})
        ])),
        ('Right joints', OrderedDict([
            ('r1', {'color': 'magenta', 'unit': '°'}),
            ('r2', {'color': 'purple', 'unit': '°'})
        ]))
    ]))
])

"""
Adjustable GUI structure, defining plots and their positions.

structure = {tabs: figure_to_plot_attributes},
figure_to_plot_attributes = {figure: plot_attributes},
plot_attributes = {'position': position, 'dimensions': dimensions}
position = (column, row)
dimensions = [dims]
dims = (dim, dim) / (dim, dim, dim)
"""
structure = OrderedDict([
    ('Overview', OrderedDict([
        ('figure1', OrderedDict([
            ('position', (0, 0)),
            ('dimensions', ['xyz'])
        ])),
        ('figure2', OrderedDict([
            ('position', (0, 1)),
            ('dimensions', [('x', 'y'), ('x', 'x'), ('x', 'z'),
                            ('y', 'x'), ('y', 'y'), ('y', 'z'),
                            ('z', 'x'), ('z', 'y'), ('z', 'z')])
        ])),
        ('figure3', OrderedDict([
            ('position', (1, 0)),
            ('dimensions', [('t', 'x'), ('t', 'y'), ('t', 'z')])
        ])),
        ('figure4', OrderedDict([
            ('position', (1, 1)),
            ('dimensions', [('t', 'α'), ('t', 'β'), ('t', 'γ')])
        ])),
        ('figure5', OrderedDict([
            ('position', (1, 2)),
            ('dimensions', [('t', 'δ'),
                            ('t', 'l1'), ('t', 'l2'),
                            ('t', 'm1'), ('t', 'm2'),
                            ('t', 'r1'), ('t', 'r2')])
        ]))
    ])),
    ('Arm', OrderedDict([
        ('figure5', OrderedDict([
            ('position', (0, 0)),
            ('dimensions', ['xyz'])
        ])),
        ('figure6', OrderedDict([
            ('position', (0, 1)),
            ('dimensions', [('x', 'y'), ('x', 'x'), ('x', 'z'),
                            ('y', 'x'), ('y', 'y'), ('y', 'z'),
                            ('z', 'x'), ('z', 'y'), ('z', 'z')])
        ])),
        ('figure7', OrderedDict([
            ('position', (1, 0)),
            ('dimensions', [('t', 'x'), ('t', 'y'), ('t', 'z')])
        ])),
        ('figure8', OrderedDict([
            ('position', (1, 1)),
            ('dimensions', [('t', DF.v('x', 'y', 'z'))])
        ]))
    ])),
    ('Hand', OrderedDict([
        ('figure9', OrderedDict([
            ('position', (0, 0)),
            ('dimensions', [('t', 'α'), ('t', 'β'), ('t', 'γ')])
        ])),
        ('figure10', OrderedDict([
            ('position', (0, 1)),
            ('dimensions', [('t', 'δ'),
                            ('t', 'l1'), ('t', 'l2'),
                            ('t', 'm1'), ('t', 'm2'),
                            ('t', 'r1'), ('t', 'r2')])
        ]))
    ]))
])


"""
Adjustable time dimension.
"""
dim_to_property = OrderedDict([('t', {'color': 'black', 'unit': 's'})])

"""
Completion of dimensional properties, please keep unchanged.
"""
for dim_group_to_dim in dimgroups.values():
    for dim_of_group_to_property in dim_group_to_dim.values():
        for dim, property_to_value in dim_of_group_to_property.items():
            if 'color' not in property_to_value:
                property_to_value['color'] = default_color
            else:
                color = property_to_value['color']
                if not isinstance(color, str):
                    if all(isinstance(c, int) for c in color) and max(color) > 1:
                        color = tuple(c/255 for c in color)
                    color = to_hex(color)
                    property_to_value['color'] = color

            dim_to_property[dim] = property_to_value

for dim in DF.dim_to_function_and_args.keys():
    dim_to_property[dim] = {'color': default_color}
