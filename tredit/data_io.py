"""
Data module containing methods to read (csv like) files and output/override files.
Also included are methods for data extracting and updating and data backup handling.
The global module variables are dataframe for saving the data from the csv file as a pandas dataframe,
indices which keep track of the selection made on the data, float indices which keep track of the precise (unrounded)
selection position, backup which stores a copy of the dataframe when needed and file_tuple (path, name, type) to
remember the last read/written file for saves.

:author: Tim Alexis Körner
"""
import subprocess
import numpy as np
import pandas as pd

from tredit.functions import DimFunctions
import tredit.config as config

dataframe = None
indices = [None, None]
float_indices = [None, None]
backup = None
file_tuple = ('./data/', 'trajectory_edit', '.csv')


def has_selection():
    """
    Checks if a complete selection exists. This corresponds to having a (not None) second indice of the selection.

    :return: Boolean representing if a complete selection exists.
    """
    global indices
    return (dataframe is not None) and (indices[1] is not None)


def create_backup():
    """
    Creates a backup made from a copied dataframe, indices and float indices.
    """
    global dataframe, indices, float_indices, backup
    backup = (dataframe.copy(), indices.copy(), float_indices.copy())


def restore_backup():
    """
    If backup exists, restores the copied dataframe, indices and float indices from backup.

    :return: Boolean representing if backup exists therefore if the restore operation was successful.
    """
    global dataframe, indices, float_indices, backup
    if backup is not None:
        dataframe, indices, float_indices = backup
        return True
    return False


def delete_backup():
    """
    Deletes the backup.
    """
    global backup
    backup = None


def get_singular_value(dim, index, calculate_modulo=False):
    """
    Returns a single value of a dim / column at the specified index.
    Also calculates modulo on the value if wanted and possible for the dim.

    :param dim: Column of the value in the dataframe.
    :param index: Index / row of the value in the dataframe.
    :param calculate_modulo: Boolean representing if the respective modulo should be calculated. Default is False.
    :return: Single value at the specified position in the dataframe.
    """
    global dataframe
    singular_value = dataframe.at[index, dim]

    if calculate_modulo and config.show_modulo and 'modulo' in config.dim_to_property[dim]:
        singular_value = np.mod(singular_value, config.dim_to_property[dim]['modulo'])

    return singular_value


def get_dim_data(dim, copy=False, calculate_modulo=False):
    """
    Returns a numpy array of a single dim / column. This can be a copy.
    Also calculates modulo on the data if wanted and possible for the dim.

    :param dim: Column in the dataframe to return.
    :param copy: Boolean representing if a copy should be returned. Default is False.
    :param calculate_modulo: Boolean representing if the respective modulo should be calculated. This entails copy.\
    Default is False.
    :return: Numpy array of a single dim / column.
    """
    global dataframe
    dim_data = dataframe[dim].to_numpy(dtype=np.float64)

    if copy or calculate_modulo:
        dim_data = dim_data.copy()

    if calculate_modulo and config.show_modulo and 'modulo' in config.dim_to_property[dim]:
        dim_data = np.mod(dim_data, config.dim_to_property[dim]['modulo'])

    return dim_data


def get_dims_data(dims, copy=False, calculate_modulo=False):
    """
    Returns a numpy array of multiple dims / columns. This can be a copy.
    Also calculates modulo on the data if wanted and possible for each dim.

    :param dims: Columns in the dataframe to return.
    :param copy: Boolean representing if a copy should be returned. Default is False.
    :param calculate_modulo: Boolean representing if each respective modulo should be calculated. This entails copy.\
    Default is False.
    :return: Numpy array of a single dim / column.
    """
    dims_data = get_dataframe(dims).to_numpy(dtype=np.float64)

    if copy or calculate_modulo:
        dims_data = dims_data.copy()

    if calculate_modulo and config.show_modulo:
        for dim_index, dim in enumerate(dims):
            if 'modulo' in config.dim_to_property[dim]:
                dims_data[dim_index] = np.mod(dims_data[dim_index], config.dim_to_property[dim]['modulo'])
    return dims_data


def get_dataframe(columns, copy=False):
    """
    Gets the dataframe of the specified columns.

    :param columns: Columns of the returned dataframe.
    :param copy: Boolean representing if the returned dataframe should be a copy. Default is False.
    :return: A part of the original dataframe, possibly copied.
    """
    global dataframe
    dataframe_part = dataframe.loc[:, columns]
    if copy:
        return dataframe_part.copy()
    return dataframe_part


def update_dataframe(new_dataframe):
    """
    Updates the dataframe with another dataframe. Duplicate columns are overwritten with the new dataframe column.
    If the new dataframe is only missing a time column, a new time column is created.

    :param new_dataframe: New dataframe, the dataframe is updated with.
    """
    global dataframe
    inverted_columns = [column for column in dataframe.columns if column not in new_dataframe.columns]
    if inverted_columns == ['t']:
        dataframe = new_dataframe.assign(t=create_time_series(len(new_dataframe.index)))
    else:
        inverted_dataframe = get_dataframe(inverted_columns)
        dataframe = new_dataframe.join(inverted_dataframe)


def update_indices(new_float_indices):
    """
    Updates the indices and float indices with the given float indices, sorted and possibly rounded.

    :param new_float_indices: Positions of the selection.
    """
    global indices, float_indices
    float_index1, float_index2 = new_float_indices
    if float_index1 is None:
        if float_index2 is None:
            float_indices = [None, None]
            indices = [None, None]
        else:
            float_indices = [float_index2, None]
            indices = [int(round(float_index2)), None]
    elif float_index2 is None:
        if float_index1 is None:
            float_indices = [None, None]
            indices = [None, None]
        else:
            float_indices = [float_index1, None]
            indices = [int(round(float_index1)), None]
    else:
        index1 = int(round(float_index1))
        index2 = int(round(float_index2))
        if index1 < index2:
            indices = [index1, index2]
            float_indices = [float_index1, float_index2]
        else:
            indices = [index2, index1]
            float_indices = [float_index2, float_index1]


def update(new_data, new_columns, new_indices=None, new_float_indices=None):
    """
    Updates the dataframe with a new dataframe made from the new data and columns.
    Also updates indices and float indices if possible.

    :param new_data: Numpy Array for the columns.
    :param new_columns: Column labels.
    :param new_indices: New indices overwriting the old ones. Default is None.
    :param new_float_indices: New float indices overwriting the old ones. Default is None.
    """
    new_dataframe = pd.DataFrame(data=new_data, columns=new_columns)
    update_dataframe(new_dataframe)

    if new_indices is not None:
        new_float_indices = [float(new_indices[0]), float(new_indices[1])]
    update_indices(new_float_indices)


def create_file_tuple(file):
    """
    Separates file path, name and type suffix for the read and output data operations.

    :param file: Complete file path.
    :return: Tuple of file_path, file_name, file_type
    """
    if file.rfind('/') != -1:
        file_path = file[: file.rfind('/') + 1]
        file = file[file.rfind('/') + 1:]
    else:
        file_path = './data/'

    if file.rfind('.') != -1:
        file_type = file[file.rfind('.'):]
        file = file[: file.rfind('.')]
    else:
        file_type = '.csv'

    if file:
        if file.endswith('_complete'):
            file = file[:len(file) - len('_complete')]
        file_name = file
    else:
        file_name = 'trajectory_edit'
    return file_path, file_name, file_type


def read_data(file):
    """
    Reads a file with the given file path into the dataframe. If a dim (column) has a modulo value specified,
    the modulo is reversed. Also adds a time dim / column and dim functions (coulumns dependent on other dims).

    :param file: File path name from where to retrieve the data.
    """
    global dataframe
    dim_to_property = config.dim_to_property.copy()
    dim_to_property.pop('t')
    dims = list(dim for dim in dim_to_property.keys() if dim not in DimFunctions.dim_to_function_and_args.keys())

    dataframe = pd.read_csv(file, delim_whitespace=True, dtype=np.float64, names=dims)
    dataframe = dataframe.assign(t=create_time_series(len(dataframe.index)))

    for dim, property_to_value in dim_to_property.items():
        if 'modulo' in property_to_value:
            reverse_modulo(dataframe[dim].to_numpy(), property_to_value['modulo'])

    calc_dim_functions(dataframe.columns)


def output_data(file=None, simulation=False):
    """
    Outputs or overrides a file with the dataframe without the columns of time and dim functions.
    If no file is given, the previous or default file will be used. Otherwise the new file path gets saved for future
    output operations. If the output is for simulation a hidden file will be used.
    If a dim has a respective modulo value, the modulo operation will be calculated
    with the resulting values centered around zero. The values are rounded to 6 decimal places before output and will
    be separated by a tab in the output file. Depending on config, the function will output
    two additional files with arm and hand columns separated.

    :param file: File in which to output the data. Default is None.
    :param simulation: Outputs a hidden file for simulation the data folder.
    :return: Lists of paths of output files
    """
    global dataframe, file_tuple
    if simulation:
        output_file_tuple = ('./data/', '.sim-data', '')
    else:
        if file is not None:
            output_file_tuple = create_file_tuple(file)
        else:
            output_file_tuple = file_tuple

    output_dataframe = dataframe.copy()

    for dim, property_to_value in config.dim_to_property.items():
        if 'modulo' in property_to_value:
            modulo = property_to_value['modulo']
            dim_data = output_dataframe[dim].to_numpy()
            dim_data = np.mod(dim_data + modulo * 1.5, modulo) - modulo/2
            output_dataframe[dim] = pd.Series(dim_data)

    output_dataframe = output_dataframe.drop('t', axis=1)
    for function_dim in DimFunctions.dim_to_function_and_args.keys():
        output_dataframe = output_dataframe.drop(function_dim, axis=1)

    output_dataframe = output_dataframe.round(6)
    
    dim_to_property = config.dim_to_property.copy()
    dim_to_property.pop('t')
    dims = list(dim for dim in dim_to_property.keys() if dim not in DimFunctions.dim_to_function_and_args.keys())

    paths = []
    if simulation or not config.split_output_data:
        paths.append(''.join(output_file_tuple))
        output_dataframe.to_csv(paths[0], columns=dims,
                                float_format='%+011.6f', sep='\t', header=False, index=False)
    else:
        for file_suffix in ['_complete', '_arm', '_hand']:
            paths.append(output_file_tuple[0] + output_file_tuple[1] + file_suffix + output_file_tuple[2])
        output_dataframe.to_csv(paths[0], columns=dims, float_format='%+011.6f', sep='\t', header=False, index=False)
        output_dataframe.to_csv(paths[1], columns=dims[:6], float_format='%+011.6f', sep='\t', header=False, index=False)
        output_dataframe.to_csv(paths[2], columns=dims[6:], float_format='%+011.6f', sep='\t', header=False, index=False)
    return paths


def calc_dim_functions(dims):
    """
    For all dim functions this method calculates the function if one of the given dims is an argument.
    Updates the dataframe with the (possibly new) column of the dim function.

    :param dims: Dims for which the dependent functions should be calculated for.
    :return: Updated dim function column names.
    """
    global dataframe
    function_dims = []
    for function_dim, (function, args) in DimFunctions.dim_to_function_and_args.items():
        if any(any(arg == dim for dim in dims) for arg in args):
            dataframe[function_dim] = function(get_dims_data(args))
            function_dims.append(function_dim)

    return function_dims


def create_time_series(size):
    """
    Creates a time column with given size.

    :param size: Length of the time column.
    :return: Time column.
    """
    return pd.Series(np.linspace(0, (size - 1) / config.frequency, size))


def reverse_modulo(column, modulo):
    """
    Reverses a modulo operation with a given modulo value on a given column in place.
    This is done by finding jumps greater then half of the modulo value and repairing them by shifting.

    :param column: Column of the dataframe to reverse the modulo operation on.
    :param modulo: Modulo value for the column.
    """
    diff_column = column[:-1] - column[1:]
    index_p = np.where(diff_column > modulo / 2)[0]
    index_m = np.where(diff_column < -modulo / 2)[0]
    index_p += 1
    index_m += 1

    n = np.zeros(len(column))
    for index in index_p:
        n[index:] += 1
    for index in index_m:
        n[index:] -= 1

    column += modulo * n


def start_simulation(sim_file=None):
    """
    Calls the run_rosnode.sh file at location <config.simulation_path>. The .sh file is then going to execute a rosnode
    python script via the "rosrun" command.

    :param sim_file: Filepath of the file that will be simulated
    :Author: Henrik Trommer
    """
    if sim_file is None:
        sim_file = output_data(simulation=True)[0]
        print(sim_file)
    subprocess.Popen([config.simulation_path, sim_file])
