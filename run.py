#!/usr/bin/env python3
"""
Script to start the GUI.
"""

from sys import argv, exit
from PyQt5.QtWidgets import QApplication
from PyQt5.QtCore import Qt

import tredit.pickercalculations as pickcalc
from tredit.userinterface import GUI

if __name__ == '__main__':
    q_app = QApplication(argv)
    q_app.setAttribute(Qt.AA_DisableWindowContextHelpButton)
    pickcalc.set_dpi(q_app)
    gui = GUI()
    exit(q_app.exec_())
