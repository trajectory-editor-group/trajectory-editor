# Projektpraktikum: Robotic Arm Trajectory GUI
## Installation
Please install all dependencies in "dependencies.txt" via pip3.
## Usage & Simulation Setup
Please refer to the Manual.
## Authors
Henrik Trommer, Tim Alexis Körner, Fabian Schabram
