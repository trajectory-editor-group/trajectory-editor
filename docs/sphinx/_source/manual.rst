Projektpraktikum
Universität Göttingen

--------------

|
| **User Manual
  for the Trajectory Editor**

--------------

General usage
=============

| Before starting the program for the first time all **Dependencies**
  (see file "dependencies.txt") must be installed via pip3 (or pip).
| To **Run** the program execute the shell script with "./run.sh" or
  start python directly with the command "python3 run.py". At startup
  the user will be prompted to open a data file with the recorded
  trajectory. All common data formats can be used. Afterwards the data
  is loaded and plotted.
| At the very top of the user interface is the **Menu Bar**, consisting
  of *File* options (save / open), *Edit* operations (undo / redo),
  *Functions* with the later explained filters and trajectory generation
  functions and *Simulation* for starting a simulation via ROS.
| There are 3 **Tabs** at the top under the menu bar: *Overview*, *Arm*
  and *Hand*, each containing multiple plots.
| At the bottom of the user interface a **Status Bar** is displaying
  context-dependent helpful information.
| To access the **Documentation** open the file
  "./docs/html/index.html". All HTML files of the documentation can be
  opened independently, but it makes sense to open the index file since
  it serves as an overview. One can also use the LATEX files in
  "./docs/latex".
| A complete overview of all mouse controls is in Table
  `1 <#mousecontrols>`__.

.. container::
   :name: mousecontrols

   .. table:: Mouse controls.

      +--------------+---------------+---------------------------------+
      | Mouse Button | Action        | Effect                          |
      +--------------+---------------+---------------------------------+
      | Left         | Click         | Choose start / end of Selection |
      +--------------+---------------+---------------------------------+
      | Left         | Double click  | Select everything               |
      +--------------+---------------+---------------------------------+
      | Left         | Gold and Drag | Move start / end of Selection   |
      +--------------+---------------+---------------------------------+
      | Right        | Click         | Delete Selection                |
      +--------------+---------------+---------------------------------+
      | Right        | Double Click  | Rescale the plot                |
      +--------------+---------------+---------------------------------+
      | Wheel        | Scroll        | Zoom in / out                   |
      +--------------+---------------+---------------------------------+
      | Wheel        | Double Click  | Rescale the plot                |
      +--------------+---------------+---------------------------------+
      | Wheel        | Hold and Drag | Move the plot                   |
      +--------------+---------------+---------------------------------+


Keyboard shortcuts
==================

There are a number of keyboard shortcuts connected to menu bar options
to accelerate the workflow. All available shortcuts can be found in
Table `2 <#shortcuts>`__.

.. container::
   :name: shortcuts

   .. table:: Keyboard shortcuts.

      =============== ================================================
      **Shortcut**    **Effect**
      =============== ================================================
      Ctrl + O        Open a new file
      Ctrl + S        Save file
      Ctrl + Shift+ S Save file as
      Ctrl + H        Open the manual
      Ctrl + C        Close the GUI
      Ctrl + Z        Undo the last change
      Ctrl +Shift +Z  Reverse the last undo
      Ctrl + 1        Generation of linear point-to-point trajectories
      Ctrl + 2        Exponential smoothing
      Ctrl + 3        Savitzky-Golay filter smoothing
      Ctrl + 4        Hand trajectory generation
      Ctrl + 5        Cut
      Ctrl + 6        Change of motion speed
      Ctrl + 7        Stop
      Ctrl + 8        Trajectory amplification
      Ctrl + Alt + 1  Start simulation
      Ctrl + Alt + 2  Choose a file for simulation
      =============== ================================================

Trajectory modification and generation functions
================================================

Function distribution
---------------------

In Table `3 <#functiondistribution>`__ the robot parts and data
columns affected by each trajectory modification or generation can be
seen.

.. container::
   :name: functiondistribution

   .. table:: Functions and affected robotic joints/columns.

      +--------------------------------------------------------------------------------------------------+-------------+--------------+
      | Functions                                                                                        | Robot parts | Data Columns |
      +--------------------------------------------------------------------------------------------------+-------------+--------------+
      | Generation of linear point-to point trajectories, Trajectory smoothing,Trajectory amplification, | Arm         | 1-6          |
      | Savitzky-Golay filter                                                                            |             |              |
      +--------------------------------------------------------------------------------------------------+-------------+--------------+
      | Hand trajectory generation                                                                       | Hand        | 7-13         |
      +--------------------------------------------------------------------------------------------------+-------------+--------------+
      | Change of motion speed, Cut, Stop                                                                | All         | All          |
      +--------------------------------------------------------------------------------------------------+-------------+--------------+

Generation of linear point-to-point trajectories
------------------------------------------------

Replaces the selected data of the arm with a linear function between the
first and last point.

Exponential smoothing
---------------------

This filter smooths selected data of the arm. A parameter determines how
much the data changes, with values closer to one increasing the effect.

Savitzky-Golay filter smoothing
-------------------------------

The Savitzky-Golay Filter resamples the selected data by polynomial
regression. A *Scipy* function is used for this.

Hand trajectory generation
--------------------------

Sets all finger data in the in the selected area to the value of the
first or last selected data point.

Cut
---

Deletes the selected range of values.

Change of motion speed
----------------------

Can be used to accelerate or slow down the movement by a factor in the
selected area. A value of 1 results in no changes. A spline
interpolation is computed to determine the resulting datapoints via
resampling.

Stop
----

Stops all motions for the selected time.

Trajectory amplification
------------------------

Multiplies selected data of the robot arm with the given factor. This
can be used to completely change parts of the robot arm’s movement.

Simulation
==========

In order to test trajectories, it is possible to simulate them via
*gazebo*. *Gazebo* must be launched with the **roslaunch** command
before starting the simulation. After the program started successfully
one can click on either "Start Simulation" or "Choose File" in the
**Simulation** menu in the trajectory editor. **Start Simulation** will
run the last save of the currently opened file in the simulator.
**Choose File** asks the user to select a specific file to be run in the
simulator, while leaving the workspace in the trajectory editor
unchanged.

ROS Setup
---------

| In order to setup the simulator, install **ROS Melodic**. The **ROS
  Workspace** that contains the scene with the robotic arm is also
  necessary. If the simulator and the scene can be launched
  successfully, the last step is to overwrite the filepath in
  *run_rosnode.sh* with the Python 2 script in the *ui_package* folder
  of the **ROS Workspace**.

Tabs
====

In this section the default tabs are listed. They can be changed in the
configuration file.

Overview
--------

| The first tab shows 5 plots in total. In the top left is a 3D plot
  with the position of the robot hand on x, y and z-axis.
| The plot below left shows the position trajectory projected on a
  plane, using :math:`x`, :math:`y` or :math:`z` on either axis selected
  with radio buttons.
| In the top right plot shows the position over time :math:`t` with all
  dimensions :math:`x`, :math:`y` and :math:`z` as default, but unwanted
  dimensions can be independently deselected via checkboxes.
| Directly below, in the middle, the three robot hand orientation angles
  :math:`\alpha`, :math:`\beta` and :math:`\gamma` are plotted against
  time :math:`t`. Each angle can again be toggled with checkboxes.
| The bottom right plots all fingers joints (:math:`\delta` for the
  spread angle in between the left and right fingers, :math:`l1` and
  :math:`l2` left joints, :math:`m1` and :math:`m2` middle joints,
  :math:`r1` and :math:`r2` right joints) with checkboxes against time
  :math:`t`.

Arm
---

The second tab has the same graphs as the overview, but instead of the
two plots for the hand orientation and finger joints there is a velocity
plot that displays :math:`v(x,y,z)` of the hand position over time
:math:`t`.

Hand
----

The last tab only shows the two plots for the hand orientation and
finger joints which were included in the *Overview* tab but omitted from
the *Arm* tab.

Configuration
=============

Adjustable Parameters
---------------------

In the module "./tredit/config.py" are multiple adjustable settings that
can not be changed within in the GUI. They appear in the first lines of
the file and are listed below.

-  split_output_data (creates three files instead of one on data output:
   complete, hand, finger)

-  show_exit_window (opens a confirmation window upon closing)

-  statusbar_timer (duration the status bar is shown)

-  simulation_path (path to the simulator start script with simulation
   file path handed as argument)

-  show_modulo (display modulo data continuously)

-  rescale_on_ok (rescales plots after filters are applied)

-  fontsize (general text size)

-  default_color (for plotted graphs)

-  max_save_count (the maximum amount of saved data states using undo
   and redo)

-  frequency (data sampling frequency)

Changing The GUI Structure
--------------------------

| All used variables can be adjusted (e.g. adding or changing units,
  color) within the dictionary *dimgroups*.
| Additionally, the whole tab structure including plot positions and
  plotted graphs can be changed using the *structure* dictionary. It is
  also possible to add or remove tabs.
| Please keep in mind that each the dimension variable used in a graph
  in *structure* must have a corresponding entry in the *dimgroups*
  dictionary.

Adding A New Function
---------------------

With the help of the configuration file it is possible to add plots with
a new function, for example named :math:`h`. In the configuration for
the velocity plot (*structure* :math:`\rightarrow` *Hand*
:math:`\rightarrow` *figure8* :math:`\rightarrow` *dimensions*) the
function :math:`v` from the class *DimFunctions* (*DF*) in the
*functions* module is called. This can be replicated by copying the call
for the new function "DF.h(…)" and adding it to the dimensions for the
plot. The function :math:`h` has to be added in the
"./tredit/functions.py" file in the *DimFunctions* class using python
language. The function decorators ("@staticmethod" and
"@dim_function_decorator.__func__") and the function parameter
("dims_array") should be used exactly like in the :math:`v` function.
The :math:`h` function can contain all kinds of array operations, as
long as the shape of returned array is the same as the given array. For
further explanation, please read the commentary for the *DimFunctions*
class.
