Documentation for the Trajectory Editor
=======================================


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   manualpage
   modules

..
  comment: Add or remove manual in 'Contents' as needed, keep 1 newline above

UML Diagram
-----------
.. image:: ./UML.png

Gitlab link
-----------
`<https://gitlab.gwdg.de/trajectory-editor-group/trajectory-editor>`_

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
